.. _mc-home:

Machine Characterization
========================

Introduction 
------------

The Machine Characterization tool is used to define the machine 
characteristics and features in term of dimensions, power absorption, cost, mass, etc.
The module is a special type of Deployment Design Tool, and it should be used
at the start of a DTOcean+ project together with the Site Characterization module.

Structure
---------

This module's documentation is divided into four main sections:

- :ref:`mc-tutorials` to give step-by-step instructions on using the Machine Characterization toll for new users. 

- :ref:`mc-how-to` that show how to achieve specific outcomes using the Machine Characterization tool. 

- A section on :ref:`background, theory and calculation methods <mc-explanation>` that describes how Machine Characterization works and aims to give confidence in the tools. 

- The :ref:`API reference <mc-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The main purpose of the Machine Characterization tool is to collect information 
about the machine to be deployed in the array. The data is subdivided in three 
categories:

- General – includes system mass, materials, rated capacity, installation depth, etc.

- Dimensions - includes linear dimensions, areas and volumes

- Model - define the main features of the machine related to its power production, such as Cp (Tidal) and CWR (Wave).

The Machine Characterization tool can be run in three differen mode, simplfified (complexity 1), 
intemedieate (complexity 2), and advanced (complexity 3) .

While the General and Dimensions inputs do not change with the mode (complexity), the Model section depends on it.

For the simplified analysis, only a single value efficiency is required for the machine.
For the intermediate analysis, an efficiency function or multi-parameters model is requried for the machine.
For the advance analysis, a more complex model is required. 


Workflow for using the Machine Characterization module
------------------------------------------------------

The workflow for using the Machine Characterization module can be summarised as 
1) provide General inputs
2) provide Dimensions inputs
3) provide Model inputs
4) lock the data to ensure results consistency

The case of advanced analysis of a wave energy converter, the workflow is modified as follow:
3.a) provide Model inputs
3.b) estimate linear potential theory coefficients using a BEM solver.



