.. _mc-business-logic:

Business Logic
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

Wave Energy Converter Complexity 3
----------------------------------

.. automodule:: dtop_machine.business.wave_converter_complexity3.power_performance
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dtop_machine.business.wave_converter_complexity3.iBEM
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dtop_machine.business.wave_converter_complexity3.multibody_analysis
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dtop_machine.business.wave_converter_complexity3.transfer
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dtop_machine.business.wave_converter_complexity3.WECComplexity3
   :members:
   :undoc-members:
   :show-inheritance:


