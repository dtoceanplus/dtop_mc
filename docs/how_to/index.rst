.. _mc-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Machine Characterization module. 
These guides are intended for users who have previously completed all the :ref:`Machine Characterizaton tutorials <mc-tutorials>` and have a good knowledge of the features and workings of the Machine Characterization module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

#. :ref:`mc-how-to-hydrodynamic`
#. :ref:`mc-how-to-default`

.. toctree::
   :maxdepth: 1
   :hidden:

   hydrodynamic_coefficients
   default_db
