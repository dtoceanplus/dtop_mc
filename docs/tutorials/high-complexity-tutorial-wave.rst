.. _mc-high-complexity-tutorial-wave: 

Wave Tutorial - RM3 case
========================

In this tutorial we will walk true the creation of a MC entity relative to the Sandia Reference Model 3 machine (RM3). 
The RM3 is a self-reacting two bodies point absorbed wave energy converter, composed by a spar and a toroidal floater.
The two bodies are mechanically constrained to have only a relative translation (prismatic joint) in the vertical axis between each other.
Full-scale dimensions and mass properties of the RM3 are shown below.

.. figure:: /figs/RM3_Geom.png
   :width: 300pt
   :align: center

1. Create/open study
---------------------

#.  Create a new complexity level 3 MC entity following the DTOcean+ main page instructions.

#.  Click ``Open`` to open the entity page.

.. Note:: If the MC entity page is refreshed for any reason, the system cannot retrieve the requried information. Please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

.. Note:: The data provided in the three input pages can be changed until the data is not locked. Once locked the no modification can be saved to the database and therefore included permanently into the entity data.

2. Enter General Inputs
-----------------------
Once opened, the MC module will show automaticaly the General inputs page for the specific entity.
At the bottom of the page is possible to find a quick summary of the entity title, machine type and complexity and 
entity ID. 

.. Note:: If the data at the bottom of the page does not correspond to the expected values, please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

#. Fill the General inputs form with the data provided in the following table:

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Cost                              | EUR     | 2000000  |
+-----------------------------------+---------+----------+
| Flating                           | -       | True     |
+-----------------------------------+---------+----------+
| Preferred Fundation Type          | -       | Drag     |
|                                   |         | Embedded |
+-----------------------------------+---------+----------+
| Connector Type                    | -       | Dry      |
+-----------------------------------+---------+----------+
| Rated Capacity                    | kW      | 286      |
+-----------------------------------+---------+----------+
| Min Installation Water Depth      | m       | -100.0   |
+-----------------------------------+---------+----------+
| Max Installation Water Depth      | m       | -40.00   |
+-----------------------------------+---------+----------+
| Material: Type                    | -       | undefined|
+-----------------------------------+---------+----------+
| Material: Quantity                | kg      | 1000000  |
+-----------------------------------+---------+----------+

3. Enter Dimensions Inputs
--------------------------

#. Click ``Dimensions`` to move to the Dimensions Inputs page.

#. Fill the Dimensions inputs form with the data provided in the table below

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Characteristic Dimension          | m       | 20       |
+-----------------------------------+---------+----------+
| Total Height                      | m       | 42       |
+-----------------------------------+---------+----------+
| Total Width                       | m       | 6        |
+-----------------------------------+---------+----------+
| Total Length                      | m       | 6        |
+-----------------------------------+---------+----------+
| Draft                             | m       | 30       |
+-----------------------------------+---------+----------+
| Total Mass                        | kg      | 1000000  |
+-----------------------------------+---------+----------+
| Submerged Volume                  | mc/m3   | 1000     |
+-----------------------------------+---------+----------+
| Footprint Radius                  | m       | 20       |
+-----------------------------------+---------+----------+
| Wet Area                          | mq/m2   | 2400     |
+-----------------------------------+---------+----------+
| Wet Frontal Area                  | mq/m2   | 240      |
+-----------------------------------+---------+----------+
| Dry Frontal Area                  | mq/m2   | 10       |
+-----------------------------------+---------+----------+


4. Enter Model Inputs
--------------------------

#. Click ``Model`` to move to the Model Inputs page.

#. Select ``Multibody`` for the machine type. Other options include Single Rigid Body or OWC.

#. Fill the Model: General Definitions tab with the data provided in table-1_.

   .. _table-1:
   .. list-table:: Model: General Definitions Tab
      :widths: 50 25 25 
      :header-rows: 1
      :width: 100 %
   
      * - Name
        - Unit
        - Value
      * - Number of wave angles
        - -
        - 14
      * - Number of wave frequencies
        - -
        - 100
      * - Minimum Wave Frequency
        - rad/s
        - 0.05
      * - Maximum Wave Frequency
        - rad/s
        - 1.5  
      * - Heading Angle Span
        - deg
        - 0
      * - Water Depth
        - m
        - 80.0
      * - Free Body DOFs
        - -
        - Surge, Heave, Pitch
      * - Estimate Farm Interaction Matrixes
        - -
        - True

#. Fill the Model: Body Definitions tab with the data provided in table-2_. and table-3_.
   Add a body at the time usign the button ``Add Body`` and click ``Confirm`` when the specific form is correctly filled.
   You will be asked to provide a mesh file of the body. (Click to download the files :download:`Spar </data/RM3_spar.GDF>` and :download:`Float </data/RM3_float.GDF>`).
   Please refer to `WAMIT <https://www.wamit.com/manualv7.4/wamit_v74manual.html>`_ 
   or `Nemoh <https://lheea.ec-nantes.fr/valorisation/logiciels-et-brevets/nemoh-presentation>`_ documentations
   for further informations about the correct mesh format.

   .. Warning:: 
      The mesh creation is probably the most critical step in the process. Please, take your time to understand the format and check the file before uploading it.
      Several alternatives can be used to create the mesh such as Rhino3D, FreeCAD or MeshLab to cite few.
      The mesh panels should be relatively homogeneous in size, and the panel normals must point toward the fluid.
      As a rule of thumb the larger panel dimensions should be at least 8-times smaller than the wave lenght associated to the max frequency defined in the previous tab.

   .. _table-2:

   +-------+---------------+
   | Body  | Mass (kg)     |
   +=======+===============+
   | Float | 727010        |
   +-------+---------------+
   | Spar  | 878300        |
   +-------+---------------+
   
   .. _table-3:
   
   +-------+-----------+------------------------+--------------------------------------+
   | Body  | Direction | Center of Gravity* (m) | Moment of Inertia Tensor (kg m^2)    |
   +=======+===========+========================+============+============+============+
   |       |    x      |  0                     | 20,907,301 | 0          | 0          |
   |       +-----------+------------------------+------------+------------+------------+
   | Float |    y      |  0                     | 0          | 21,306,091 | 0          |
   |       +-----------+------------------------+------------+------------+------------+
   |       |    z      | -0.72                  | 0          | 0          | 37,085,481 |
   +-------+-----------+------------------------+------------+------------+------------+
   |       |    x      |  0                     | 94,419,615 | 0          | 0          |
   |       +-----------+------------------------+------------+------------+------------+
   | Spar  |    y      |  0                     | 0          | 94,407,091 | 0          |
   |       +-----------+------------------------+------------+------------+------------+
   |       |    z      | -21.29                 | 0          | 0          | 28,542,225 |
   +-------+-----------+------------------------+------------+------------+------------+

   .. Warning::
      Make sure that the Spar body has ID = 0, by dragging the table row. This is important since the ID=0 is reserved to the system "platform" for the multibody analysis.

#. Fill the Model: Joint Definitions tab with the following data.

   .. list-table:: Model: Joints Definition
      :header-rows: 1
      :width: 100 %

      * - Type
        - Parent
        - Child
        - Axis of Application
        - Point of Application
      * - Prismatic
        - 0
        - 1
        - [0,0,1]
        - [0,0,0]

#. Use the deault values in the Model: Performance Definitions tab and upload the Power Performance Data using the ``Upload XLS`` button.
   The data relative to the RM3 machine can be downloaded by clicking :download:`CWR Matrix </data/CWR_matrix.xlsx>`

#. Fill the Model: PTO Definitions tab with the following data:

   * Surge -> Average Mooring Stiffness -> 1000.0

   * prismatic0 -> Average PTO Damping -> 1200000

#. Click ``Submit`` to save the data to the database.

#. Click ``Calculate`` and follow the instructions to estimate the hydrodynamic coefficients for the machine. Due to the number of problem to solve, this can take up to 2-5 hours.
   
   .. Note::
      Please before submit the full calculation, run the module with a limited number of Wave Directions and Wave Frequency, for example 3, 3.
      This allow the user to verify the validity of the data and correct any errors without losing excessive time.


5. Lock the Machine Data
--------------------------

Once all the data has been properly entered, it is important to lock the data for the entity to complete the project.

.. Note:: If the data is not locked the project status will not reach 100% and this will prevent other modules to consume the input data.

The data can be locked by clicking the padlock on the right hand side of the page, under the status bar.
Once the data is locked the status bar becomes green and the project status is set to 100% or completed, at this stage the data
cannot be changed anylonger.

