.. _mc-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Machine Characterization tool.
They are intended for those who are new to the Machine Characterization tool.

.. It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

Since the Machine Characterization tool does not depend on any other DTOcea+ tools, the module usage will not different between integrated
or standalone mode.

**Using the Machine Characterization tool**

Since the functinalities and workflow of the Machine Characterization tool is the same for all the case, except for the analysis of a 
wave energy converter at complexity 3 (advanced mode), three tutorials are provided for these separate cases.

#. :ref:`Using MC at high complexity for RM1 tidal converter <mc-high-complexity-tutorial-tidal>`
   This tutorial not only covers the specific analysis of Tidal machine at complexity 3, but any other case except Wave at complexity 3.

#. :ref:`Using MC at high complexity for Heaving Cyinder wave converter <mc-high-complexity-tutorial-wave-cyl>`

#. :ref:`Using MC at high complexity for RM3 wave converter <mc-high-complexity-tutorial-wave>`

.. toctree::
   :maxdepth: 1
   :hidden:

   high-complexity-tutorial-tidal
   high-complexity-tutorial-wave-cyl
   high-complexity-tutorial-wave
   
