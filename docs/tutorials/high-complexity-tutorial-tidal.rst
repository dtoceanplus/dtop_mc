.. _mc-high-complexity-tutorial-tidal: 

Tidal Tutorial - RM1 case
=========================

In this tutorial we will walk true the creation of a MC entity relative to the Sandia Reference Model 1 machine (RM1). 
The RM1 is a dual rotors bottom fixed tidal turbine. Each rotor is a standard horizontal axis three blade tidal turbine.

1. Create/open study
---------------------

#.  Create a new complexity level 3 MC entity following the DTOcean+ main page instructions.

#.  Click ``Open`` to open the entity page.

.. Note:: If the MC entity page is refreshed for any reason, the system cannot retrieve the requried information. Please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

.. Note:: The data provided in the three input pages can be changed until the data is not locked. Once locked the no modification can be saved to the database and therefore included permanently into the entity data.

2. Enter General Inputs
-----------------------
Once opened, the MC module will show automaticaly the General inputs page for the specific entity.
At the bottom of the page is possible to find a quick summary of the entity title, machine type and complexity and 
entity ID. 

.. Note:: If the data at the bottom of the page does not correspond to the expected values, please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

#. Fill the General inputs form with the data provided in the following table:

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Cost                              | EUR     | 1960000  |
+-----------------------------------+---------+----------+
| Flating                           | -       | False    |
+-----------------------------------+---------+----------+
| Preferred Fundation Type          | -       | Pile     |
+-----------------------------------+---------+----------+
| Connector Type                    | -       | Wet      |
+-----------------------------------+---------+----------+
| Rated Capacity                    | kW      | 11000    |
+-----------------------------------+---------+----------+
| Min Installation Water Depth      | m       | -67.50   |
+-----------------------------------+---------+----------+
| Max Installation Water Depth      | m       | -45.00   |
+-----------------------------------+---------+----------+
| Material: Type                    | -       | undefined|
+-----------------------------------+---------+----------+
| Material: Quantity                | kg      | 219370   |
+-----------------------------------+---------+----------+

3. Enter Dimensions Inputs
--------------------------

#. Click ``Dimensions`` to move to the Dimensions Inputs page.

#. Fill the Dimensions inputs form with the data provided in the table below

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Rotor Diameter                    | m       | 10       |
+-----------------------------------+---------+----------+
| Hub Height                        | m       | 30       |
+-----------------------------------+---------+----------+
| Total Height                      | m       | 30       |
+-----------------------------------+---------+----------+
| Total Width                       | m       | 3.5      |
+-----------------------------------+---------+----------+
| Total Length                      | m       | 3.5      |
+-----------------------------------+---------+----------+
| Draft                             | m       | 0        |
+-----------------------------------+---------+----------+
| Total Mass                        | kg      | 219370   |
+-----------------------------------+---------+----------+
| Submerged Volume                  | mc/m3   | 433      |
+-----------------------------------+---------+----------+
| Footprint Radius                  | m       | 20       |
+-----------------------------------+---------+----------+
| Wet Area                          | mq/m2   | 165      |
+-----------------------------------+---------+----------+
| Wet Frontal Area                  | mq/m2   | 165      |
+-----------------------------------+---------+----------+
| Dry Frontal Area                  | mq/m2   | 0        |
+-----------------------------------+---------+----------+


4. Enter Model Inputs
--------------------------

#. Click ``Model`` to move to the Model Inputs page.

#. Fill the Model inputs form with the data provided in the table below

#. The Cp/Ct functions can be defined in two different ways:

   * Manually: add a new point to the list clicking ``Add New Point`` and edit its values. Repeat the process for each point.

   * Upload: upload an xls file clicking ``Upload XLS``. The file format is specified at the end of this tutorial together with the data for the RM1 case. 

#. Select the range of operational velocity for the machine using the slider on top of the Cp/Ct plot. Select 0.5 - 3 m/s range.

#. Click ``Submit`` to save the data to the database.

.. list-table:: Model Inputs Table
   :widths: 50 25 25 
   :header-rows: 1
   :width: 100 %

   * - Name
     - Unit
     - Value
   * - Bi-directional
     - -
     - True
   * - Number of Rotor
     - -
     - 2
   * - Rotor Interdistance
     - m
     - 10
   * - Heading Angle Span
     - deg
     - 0

For complex Cp/Ct curves it is convenient to use the upload functionality.
The data must be stored in a XLS or XLSX file and organized in three columns.

For the RM1 machine the Cp/Ct data is defined in function of the flow velocity (m/s) as:

.. csv-table:: Cp/Ct XLS file format and data
   :file: ../data/ct_cp_def.csv
   :widths: 33, 33, 34
   :header-rows: 1


5. Lock the Machine Data
--------------------------

Once all the data has been properly entered, it is important to lock the data for the entity to complete the project.

.. Note:: If the data is not locked the project status will not reach 100% and this will prevent other modules to consume the input data.

The data can be locked by clicking the padlock on the right hand side of the page, under the status bar.
Once the data is locked the status bar becomes green and the project status is set to 100% or completed, at this stage the data
cannot be changed anylonger.
