.. _mc-high-complexity-tutorial-wave-cyl: 

Wave Tutorial - Heaving Cylinder
================================

In this tutorial we will walk true the creation of a MC entity relative to the a single degree of freedom wave energy converter. 
The machine is a generic point absorber with a cylindrical body and allowed to move only in Heave.
Full-scale dimensions and mass properties of the Heaving Cylinder are shown below.

.. figure:: /figs/Cylinder.png
   :width: 300pt
   :align: center

1. Create/open study
---------------------

#.  Create a new complexity level 3 MC entity following the DTOcean+ main page instructions.

#.  Click ``Open`` to open the entity page.

.. Note:: If the MC entity page is refreshed for any reason, the system cannot retrieve the requried information. Please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

.. Note:: The data provided in the three input pages can be changed until the data is not locked. Once locked the no modification can be saved to the database and therefore included permanently into the entity data.

2. Enter General Inputs
-----------------------
Once opened, the MC module will show automaticaly the General inputs page for the specific entity.
At the bottom of the page is possible to find a quick summary of the entity title, machine type and complexity and 
entity ID. 

.. Note:: If the data at the bottom of the page does not correspond to the expected values, please close the browser tab, return to the Main DTOcean+ page and reopen the correct MC entity.

#. Fill the General inputs form with the data provided in the following table:

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Cost                              | EUR     | 2000000  |
+-----------------------------------+---------+----------+
| Flating                           | -       | True     |
+-----------------------------------+---------+----------+
| Preferred Fundation Type          | -       | Drag     |
|                                   |         | Embedded |
+-----------------------------------+---------+----------+
| Connector Type                    | -       | Dry      |
+-----------------------------------+---------+----------+
| Rated Capacity                    | kW      | 250      |
+-----------------------------------+---------+----------+
| Min Installation Water Depth      | m       | -200.0   |
+-----------------------------------+---------+----------+
| Max Installation Water Depth      | m       | -20.00   |
+-----------------------------------+---------+----------+
| Material: Type                    | -       | undefined|
+-----------------------------------+---------+----------+
| Material: Quantity                | kg      | 785000   |
+-----------------------------------+---------+----------+

3. Enter Dimensions Inputs
--------------------------

#. Click ``Dimensions`` to move to the Dimensions Inputs page.

#. Fill the Dimensions inputs form with the data provided in the table below

#. Click ``Submit`` to save the data to the database.

+-----------------------------------+---------+----------+
| Name                              | Unit    | Value    |
+===================================+=========+==========+
| Characteristic Dimension          | m       | 10       |
+-----------------------------------+---------+----------+
| Total Height                      | m       | 12       |
+-----------------------------------+---------+----------+
| Total Width                       | m       | 10       |
+-----------------------------------+---------+----------+
| Total Length                      | m       | 10       |
+-----------------------------------+---------+----------+
| Draft                             | m       | 10       |
+-----------------------------------+---------+----------+
| Total Mass                        | kg      | 785000   |
+-----------------------------------+---------+----------+
| Submerged Volume                  | mc/m3   | 785      |
+-----------------------------------+---------+----------+
| Footprint Radius                  | m       | 20       |
+-----------------------------------+---------+----------+
| Wet Area                          | mq/m2   | 2400     |
+-----------------------------------+---------+----------+
| Wet Frontal Area                  | mq/m2   | 240      |
+-----------------------------------+---------+----------+
| Dry Frontal Area                  | mq/m2   | 10       |
+-----------------------------------+---------+----------+


4. Enter Model Inputs
--------------------------

#. Click ``Model`` to move to the Model Inputs page.

#. Select ``Single Body`` for the machine type. Other options include Multibody or OWC.

#. Fill the Model: General Definitions tab with the data provided in table-4_.

   .. _table-4:
   .. list-table:: Model: General Definitions Tab
      :widths: 50 25 25 
      :header-rows: 1
      :width: 100 %
   
      * - Name
        - Unit
        - Value
      * - Number of wave angles
        - -
        - 14
      * - Number of wave frequencies
        - -
        - 100
      * - Minimum Wave Frequency
        - rad/s
        - 0.05
      * - Maximum Wave Frequency
        - rad/s
        - 1.5  
      * - Heading Angle Span
        - deg
        - 0
      * - Water Depth
        - m
        - 100.0
      * - Free Body DOFs
        - -
        - Heave
      * - Estimate Farm Interaction Matrixes
        - -
        - True

#. Fill the Model: Body Definitions tab with the data provided in table-5_. and table-6_. Please note that since the system is 
   allowed to move only in the vertical direction, the moment of inertia in irrelevant.
   Add a body at the time usign the button ``Add Body`` and click ``Confirm`` when the specific form is correctly filled.
   You will be asked to provide a mesh file of the body. (Click to download the files :download:`Cylinder </data/Cylinder.dat>`).
   Please refer to `WAMIT <https://www.wamit.com/manualv7.4/wamit_v74manual.html>`_ 
   or `Nemoh <https://lheea.ec-nantes.fr/valorisation/logiciels-et-brevets/nemoh-presentation>`_ documentations
   for further informations about the correct mesh format.

   .. Warning:: 
      The mesh creation is probably the most critical step in the process. Please, take your time to understand the format and check the file before uploading it.
      Several alternatives can be used to create the mesh such as Rhino3D, FreeCAD or MeshLab to cite few.
      The mesh panels should be relatively homogeneous in size, and the panel normals must point toward the fluid.
      As a rule of thumb the larger panel dimensions should be at least 8-times smaller than the wave lenght associated to the max frequency defined in the previous tab.

   .. _table-5:

   +----------+---------------+
   | Body     | Mass (kg)     |
   +==========+===============+
   | Cylinder | 785000        |
   +----------+---------------+
   
   .. _table-6:
   
   +------------+-----------+------------------------+--------------------------------------+
   | Body       | Direction | Center of Gravity* (m) | Moment of Inertia Tensor (kg m^2)    |
   +============+===========+========================+============+============+============+
   |            |    x      |  0                     | 1          | 0          | 0          |
   |            +-----------+------------------------+------------+------------+------------+
   | Float      |    y      |  0                     | 0          | 1          | 0          |
   |            +-----------+------------------------+------------+------------+------------+
   |            |    z      | -7.5                   | 0          | 0          | 1          |
   +------------+-----------+------------------------+------------+------------+------------+

#. Use the deault values in the Model: Performance Definitions tab and upload the Power Performance Data using the ``Upload XLS`` button.
   The data relative to the RM3 machine can be downloaded by clicking :download:`CWR Matrix </data/CWR_matrix.xlsx>`

#. Fill the Model: PTO Definitions tab with the following data:

   * Heave -> Average PTO Damping -> 286000.0

#. Click ``Submit`` to save the data to the database.

#. Click ``Calculate`` and follow the instructions to estimate the hydrodynamic coefficients for the machine. Due to the number of problem to solve, this can take up to 2-5 hours.
   
   .. Note::
      Please before submit the full calculation, run the module with a limited number of Wave Directions and Wave Frequency, for example 3, 3.
      This allow the user to verify the validity of the data and correct any errors without losing excessive time.


5. Lock the Machine Data
--------------------------

Once all the data has been properly entered, it is important to lock the data for the entity to complete the project.

.. Note:: If the data is not locked the project status will not reach 100% and this will prevent other modules to consume the input data.

The data can be locked by clicking the padlock on the right hand side of the page, under the status bar.
Once the data is locked the status bar becomes green and the project status is set to 100% or completed, at this stage the data
cannot be changed anylonger.

