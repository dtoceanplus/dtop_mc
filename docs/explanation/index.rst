.. _mc-explanation:

*********************
Background and theory
*********************

As explained in the introduction the Machine Characterization tool, only performs calculation
for the case of Wave Energy Converter at complexity 3 (advanced mode). 
For any other case the Machine Characterization module is simply a data holder, therfore no background and theory is required.

For the special case of Wave Energy Converter in the advanced mode, the Machine Characterizaiton module uses a 
Boundary Element Method (BEM) solver to estimate the hydrodynamic coefficient of the machine.
The Machine Characterization module uses an python wrapper for the Nemoh BEM solver, called Capytaine.

The BEM solves the linear potential boundary value problem for an incompressible, irrotational and inviscid fluid.
The description of the underpinning theory is behind the scope of this documentation.
The user can find all the required information at the following links:

#. `Capytaine documentation <https://ancell.in/capytaine/latest/theory_manual/index.html>`_

#. `Nemoh documentation <https://lheea.ec-nantes.fr/valorisation/logiciels-et-brevets/nemoh-presentation>`_ 

Additionaly, some more detailed information can be collected using the WAMIT manual at the following link 
( `WAMIT <https://www.wamit.com/manualv7.4/wamit_v74manual.html>`_ )

WAMIT is a commercial BEM software with excellent documentation about the background theory.

