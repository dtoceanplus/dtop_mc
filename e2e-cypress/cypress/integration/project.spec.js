import ProjectPage from '../pages/project.page';

describe("project", () => {
    const project = new ProjectPage();
    before(() => {
        project.createProject()
    })

    after(() => {
        project.removeProject()
    })

    it('Check the fixture files loaded', function () {
        let fileContents = this.generalJSON
        expect(fileContents).to.have.property('connector_type')
        expect(fileContents.connector_type).to.equal("dry")
      })

    it('Sumbit General Inputs', () => {
        project.setGeneral()
    })

    it('Sumbit Dimension Inputs', () => {
        project.visitDimension()
        project.setDimension()
    })
})