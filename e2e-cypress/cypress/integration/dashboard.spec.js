import DashboardPage from '../pages/dashboard.page';
describe("dashboard", () => {
    const dashboard = new DashboardPage();

    it('Create MC project', () => {
        dashboard.visit();
        dashboard.createProject();
    })

    it('Edit MC project', () => {
        dashboard.editProject()
    })

    it('Open MC project', () => {
        dashboard.openProject()
    })

    it('Remove MC project', () => {
        dashboard.visit();
        dashboard.removeProject()
    })
})