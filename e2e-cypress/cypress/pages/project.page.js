import DashboardPage from './dashboard.page';

class ProjectPage {
    static POWER_FACTOR = 0.3
    static MAX_WATER_DEPTH = 100
    static MIN_WATER_DEPTH = 10
    static MAX_DIST_X = 30
    static MAX_DIST_Y = 50
    static RATED_VOLT = 220
    static MATERIAL_QUANTITY = 131908
    static MACHINE_COST = 3000001
    static RATED_CAPACIY = 1000500
    static SUBMIT_SUCCESS = 201
    static SUB_VOL = 10
    static CHARCT_DIM = 1
    static DRAFT = 10121
    static MASS = 10143
    static TOT_HEIGHT = 103
    static TOT_WIDTH = 101
    static TOT_LENGTH = 11
    static WET_AREA = 1011
    static WET_FRONT_AREA = 101
    static DRY_FRONT_AREA = 102
    static BEM_WET_AREA = 141
    static FOOTPRINT_RADIUS = 1014

    constructor() {
        this.dashboard = new DashboardPage();
    }

    createProject() {
        this.dashboard.visit()
        this.dashboard.createProject()
        this.dashboard.openProject()
        
        // cy.visit('/#/mc-entity?entityID=1')
        cy.fixture('general.json').as('generalJSON')
        cy.fixture('dimensions.json').as('dimensionsJSON')
    }

    removeProject() {
        this.dashboard.visit()
        this.dashboard.removeProject()
        // cy.request('DELETE', '/mc/1')
    }

    openProject() {
      this.dashboard.visit()
      this.dashboard.openProject()
    }

    setDimension() {
        // cy.get(`[data-cy="mass"]`).type(ProjectPage.MASS);
        // cy.get(`[data-cy="characteristic_dimension"]`).type(ProjectPage.CHARCT_DIM);
        // cy.get(`[data-cy="draft"]`).type(ProjectPage.DRAFT);
        // cy.get(`[data-cy="submerged_volume"]`).type(ProjectPage.SUB_VOL);
        // cy.get(`[data-cy="total_height"]`).type(ProjectPage.TOT_HEIGHT);
        // cy.get(`[data-cy="total_width"]`).type(ProjectPage.TOT_WIDTH);
        // cy.get(`[data-cy="total_length"]`).type(ProjectPage.TOT_LENGTH);
        // cy.get(`[data-cy="wet_area"]`).type(ProjectPage.WET_AREA);

        // TODO: THE COMMENTED FIELDS ARE COMPLEXITY DEPENDENT
        // cy.get(`[data-cy="wet_frontal_area"]`).type(ProjectPage.WET_FRONT_AREA);
        // cy.get(`[data-cy="dry_frontal_area"]`).type(ProjectPage.DRY_FRONT_AREA);
        // cy.get(`[data-cy="beam_wet_area"]`).type(ProjectPage.BEM_WET_AREA);
        // cy.get(`[data-cy="footprint_radius"]`).type(ProjectPage.FOOTPRINT_RADIUS);
        
        cy.server()           // enable response stubbing
        cy.route('PUT', 'dimensions').as('putDimension')
        cy.get(`[data-cy="submit_form_dimensions"]`).click()
        
        cy.wait('@putDimension').then((xhr) => {
            console.log
            console.log(this.dimensionsJSON)
            // expect(xhr.response.body).to.equal(this.generalJSON)
            expect(xhr.response.body.message)
                    .contains("Dimensions inputs added for project")
            expect(xhr.status).to.equal(ProjectPage.SUBMIT_SUCCESS)
            // expect(xhr.url, 'post url').to.match(/\/posts$/)
            // assert any other XHR properties
        })
        
    }

    setGeneral() {
        this.fill_machine_cost(ProjectPage.MACHINE_COST)
        this.fill_rated_capacity(ProjectPage.RATED_CAPACIY)
        this.fill_constant_power_factor(ProjectPage.POWER_FACTOR)
        cy.get(`[data-cy="floating"]`).click()
        cy.get(`[data-cy="preferred_fundation_type"]`).click().type('{downarrow}{enter}')
        cy.get(`[data-cy="connector_type"]`).click().type('{downarrow}{enter}')
        cy.get(`[data-cy="max_installation_water_depth"]`).type(ProjectPage.MAX_WATER_DEPTH);
        cy.get(`[data-cy="min_installation_water_depth"]`).type(ProjectPage.MIN_WATER_DEPTH);
        cy.get(`[data-cy="min_interdistance_x"]`).type(ProjectPage.MAX_DIST_X);
        cy.get(`[data-cy="min_interdistance_y"]`).type(ProjectPage.MAX_DIST_Y);
        cy.get(`[data-cy="rated_voltage"]`).type(ProjectPage.RATED_VOLT);

        cy.get(`[data-cy="select_material"]`).click().type('{downarrow}{enter}').click().type('{downarrow}{enter}')
        cy.get(`[data-cy="select_quantity"]`).type(ProjectPage.MATERIAL_QUANTITY);
        cy.get(`[data-cy="add_material"]`).click()

        cy.server()           // enable response stubbing
        cy.route('PUT', 'general').as('putGeneral')
        cy.get(`[data-cy="submit_form"]`).click()
        
        cy.wait('@putGeneral').then((xhr) => {
            console.log
            console.log(this.generalJSON)
            // expect(xhr.response.body).to.equal(this.generalJSON)
            expect(xhr.response.body.message)
                    .contains("General inputs added to project")
            expect(xhr.status).to.equal(ProjectPage.SUBMIT_SUCCESS)
            // expect(xhr.url, 'post url').to.match(/\/posts$/)
            // assert any other XHR properties
        })
        
    }

    visitDimension() {
        cy.get('[data-cy="linkDimensions"]').click()
    }

    openLinkGeneral() {
        cy.get('[data-cy="linkGeneral"]').click()
    }

    fill_machine_cost(value) {
        const field = cy.get(`[data-cy="machine_cost"]`);
        field.type(value);
        return this;
    }

    fill_rated_capacity(value) {
        const field = cy.get(`[data-cy="rated_capacity"]`);
        field.type(value);
        return this;
    }

    fill_constant_power_factor(value) {
        const field = cy.get(`[data-cy="constant_power_factor"]`);
        field.type(value);
        return this;
    }

}
export default ProjectPage;