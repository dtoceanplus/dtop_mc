import "./commands.js"

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from failing the test
    // because of ResizeObserver loop limit exceeded
    return false
  })
