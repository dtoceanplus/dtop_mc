# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, url_for, make_response, request, jsonify
from flask import current_app as app
from datetime import datetime as dt

from dtop_machine.service.models import db, Project
from dtop_machine.service.schemas import ProjectSchema


bp = Blueprint('routes', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/', methods=['GET'])
def get_all_routes():
    result = []
    for el in app.url_map.iter_rules():
        print(el)
        result.append(str(el))

    return make_response(jsonify(result), 200, headers)
