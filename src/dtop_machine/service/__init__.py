# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# This file should contain create_app() function.
# This function is used by Flask.
import os
from os.path import abspath, dirname, normpath

from flask import Flask, request, jsonify, render_template
from flask_babel import Babel
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from .config import Config

babel = Babel()

# manual config
import logging

import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')
# logging.basicConfig(filename='/logs/mc.log', filemode='w',
#                      format='%(asctime)s :: %(name)s - %(levelname)s - %(message)s')
# logger = logging.getLogger()
# logger.setLevel(logging.INFO)
# file_handler = RotatingFileHandler('/logs/mc.log','w', 1000000,1)
# formatter = logging.Formatter('%(asctime)s :: %(name)s - %(levelname)s :: %(message)s', '%Y-%m-%d %H:%M:%S')
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

from dtop_machine.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger(__name__)

logger.warning('Started app.py!')

def create_app(test_config=None):

    # create and configure the app
    app = Flask(__name__,
                instance_relative_config=False,
                instance_path = normpath(abspath(dirname(__file__) + "../../instance"))
                )
    
    # app.logger.disabled = True
    log = logging.getLogger('werkzeug')
    log.disabled = True
    
    app.config.from_object(Config)

    CORS(app)
    babel.init_app(app)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    from . import db

    db.init_app(app)

    from .schemas import ma

    ma.init_app(app)

    # Registering Blueprints
    from . import projects as projects
    app.register_blueprint(projects.bp)

    from . import general as general
    app.register_blueprint(general.bp)

    from . import model as model
    app.register_blueprint(model.bp)
    
    from . import dimensions as dimensions
    app.register_blueprint(dimensions.bp)

    from . import inputs as inputs
    app.register_blueprint(inputs.bp)

    from . import results as results
    app.register_blueprint(results.bp)

    from . import tasks as tasks
    app.register_blueprint(tasks.bp)

    from . import representation as dr
    app.register_blueprint(dr.bp)

    from . import api as api
    app.register_blueprint(api.bp)

    from . import routes as routes
    app.register_blueprint(routes.bp, url_prefix='/routes')

    if os.environ.get('FLASK_ENV') == 'development':
        from . import provider_states as provider_states
        app.register_blueprint(provider_states.bp)

    # # This should be activated IF the front end is integrated without a separate port
    # @app.route('/', defaults={'path': ''})
    # @app.route('/<path:path>')
    # def render_vue(path):
    #     return render_template("index.html")

    return app


# @babel.localeselector
# def get_locale():
#     return request.accept_languages.best_match(['en', 'fr'])
