# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from contextlib import suppress

import sqlalchemy.exc
from flask import Blueprint, request, jsonify

from dtop_machine.service.db import init_db
from dtop_machine.service.models import General, Model, Dimensions, Results, Project, db
from dtop_machine.service.schemas import *

import json

bp = Blueprint('provider_states', __name__)


@bp.route('/provider_states/setup', methods=['POST'])
def provider_states_setup():
    """
    Reset database and create data neccessary for contract testing.
    Should be available only with `FLASK_ENV=development`.
    """
    init_db()

    consumer = request.json['consumer']
    state = request.json['state']

    if state.startswith('mc 1 exists'):
        with suppress(sqlalchemy.exc.IntegrityError):
            with db.session.begin_nested():
                project = Project(id=1, title='test',desc='test',type='WEC',complexity=1)
                db.session.add(project)

        if state == 'mc 1 exists and it has general':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    general = General(project=project)
                    body = {"floating":True,
                            "rated_capacity":1.11,
                            "max_installation_water_depth":-10.0,
                            "min_installation_water_depth":-100.0,
                            "min_interdistance_x":100.0,
                            "min_interdistance_y":50.0,
                            "connector_type": "Wet",
                            "machine_cost": 1000000,
                            "materials": "{'concrete': 1}"}  # the materials variable is needed cos is used in the Marshmallow schema
                    general.update(**body)
                    db.session.add(general)
        
        if state == 'mc 1 exists and it has dimensions':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    dimensions = Dimensions(project_id=1,
                                    characteristic_dimension=13.5,
                                    hub_height=15, mass=2.5,
                                    wet_area=2.5, height=5,
                                    width=5, length=200)
                    db.session.add(dimensions)

        if state == 'mc 1 exists and it has model wec1':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"capture_width_ratio": 0.3,
                                         "machine_archetype": "point_absorber",
                                         "pto_damping": [1000000]})
                    db.session.add(model)

        if state == 'mc 1 exists and it has model wec2':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"pto_damping": [1000000]})
                    db.session.add(model)
        
        if state == 'mc 1 exists and it has model wec3':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"pto_damping": [[[[[1000000]]]]]})
                    db.session.add(model)

        if state == 'mc 1 exists and it has tec at cpx1':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"cp": 0.3,
                                         "number_rotor": 1})
                    db.session.add(model)

        if state == 'mc 1 exists and it has tec at cpx2':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"cp": 0.3,
                                         "number_rotor": 1,
                                         "tip_speed_ratio": 1.0,
                                         "ct": 0.4,
                                         "cut_in_velocity": 0.3,
                                         "cut_out_velocity": 2})
                    db.session.add(model)

        if state == 'mc 1 exists and it has tec at cpx3':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    model = Model(project_id=1,
                                  model={"cp": [0.3,],
                                  "number_rotor": 1,
                                  "tip_speed_ratio": 1,
                                  "ct": [0.5,],
                                  "cut_in_velocity": 0.3,
                                  "cut_out_velocity": 2.1,
                                  "cp_ct_velocity": [0.2,]})
                    db.session.add(model)


    return ''
