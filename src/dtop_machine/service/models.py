# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from sqlalchemy.ext import mutable
import json

from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

class Project(db.Model):

    __tablename__ = 'project'
    id = db.Column(db.Integer,
                   primary_key=True)
    title = db.Column(db.String(100),
                         index=False,
                         unique=False,
                         nullable=False)
    desc = db.Column(db.String,
                      index=True,
                      unique=False,
                      nullable=False)
    complexity = db.Column(db.Integer)
    type = db.Column(db.Text,
                    index=False,
                    unique=False,
                    nullable=True)
    date = db.Column(db.Text,
                    index=False,
                    unique=False,
                    nullable=True)
    data_lock = db.Column(db.Boolean,
                        default=False, 
                        nullable=True)
    status = db.Column(db.Integer)
    tags = db.Column(db.Text,
                    index=False,
                    unique=False,
                    nullable=True)
    model = db.relationship('Model',
                             backref='project',
                             uselist=False,
                             cascade="all, delete-orphan")
    general = db.relationship('General',
                             backref='project',
                             uselist=False,
                             cascade="all, delete-orphan")                             
    dimensions = db.relationship('Dimensions',
                             backref='project',
                             uselist=False,
                             cascade="all, delete-orphan")
    results = db.relationship('Results',
                             backref='project',
                             uselist=False,
                             cascade="all, delete-orphan")                        

    def update(self, **kwargs):
        project_exclude = ['id', 'model', 'dimensions', 'general', 'results'] 
        for key, value in kwargs.items():
            if hasattr(self, key) and not key in project_exclude:
                setattr(self, key, value)

class JsonEncodedDict(db.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value)  

mutable.MutableDict.associate_with(JsonEncodedDict)

class General(db.Model):
    __tablename__ = 'general'
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    floating = db.Column(db.Boolean)
    connector_type = db.Column(db.String(100))
    constant_power_factor = db.Column(db.Float)
    machine_cost = db.Column(db.Float)
    max_installation_water_depth = db.Column(db.Float)
    min_installation_water_depth = db.Column(db.Float)
    min_interdistance_x = db.Column(db.Float)
    min_interdistance_y = db.Column(db.Float)
    preferred_fundation_type = db.Column(db.String(100))
    rated_capacity = db.Column(db.Float)
    rated_voltage = db.Column(db.Float)
    materials = db.Column(db.String(1000))

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)

class Model(db.Model):
    __tablename__ = 'model'
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    model = db.Column(JsonEncodedDict, default={})

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)

class Dimensions(db.Model):
    __tablename__ = 'dimensions'
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    id = db.Column(db.Integer, primary_key=True)
    characteristic_dimension = db.Column(db.Float, nullable=True)
    draft = db.Column(db.Float, nullable=True)
    hub_height = db.Column(db.Float, nullable=True)
    height = db.Column(db.Float, nullable=True)
    width = db.Column(db.Float, nullable=True)
    length = db.Column(db.Float, nullable=True)
    submerged_volume = db.Column(db.Float, nullable=True)
    wet_area = db.Column(db.Float, nullable=True)
    wet_frontal_area = db.Column(db.Float, nullable=True)
    dry_frontal_area = db.Column(db.Float, nullable=True)
    beam_wet_area = db.Column(db.Float, nullable=True)
    dry_profile = db.Column(db.String(100))
    wet_profile = db.Column(db.String(100))
    mass = db.Column(db.Float, nullable=True)
    footprint_radius = db.Column(db.Float, nullable=True)

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)

class Results(db.Model):
    __tablename__ = 'results'
    id = db.Column(db.Integer, primary_key=True) 
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)             
    results = db.Column(JsonEncodedDict, default={})

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)

class Task(db.Model):
    __tablename__ = 'tasks'
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    rq_job_id = db.Column(db.String, nullable=False)
    status = db.Column(db.String, nullable=True)
    message = db.Column(db.String, nullable=True)


