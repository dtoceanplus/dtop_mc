# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, make_response, url_for, request, jsonify

from dtop_machine.service.models import *
from dtop_machine.service.schemas import *

bp = Blueprint('general', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/mc/<id>/general', methods=['PUT'])
def put_general(id):

    try:
        project = Project.query.filter_by(id=id).first()
        if project is None:
            raise ValueError(f'Invalid project ID: {id}')
        body = request.get_json()
        materials = body.pop('materials')
        if project.general is None:
            general = General(**body, project=project)
        else:  
            general = project.general
            general.update(**body)

        general.materials = json.dumps(materials)
                    
        db.session.add(general)
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': f'General inputs added to project {id}'}),
                             201,
                             headers)
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

@bp.route('/mc/<id>/general', methods=['GET'])
def get_general(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        general = project.general
        general_body = json.loads(general_schema.dumps(general))
        return make_response(jsonify(general_body),
                                200,
                                headers)
    except:
        
        return make_response(jsonify({'message': f'No general data has been saved for the Project ID {id}'}),
                             404,
                             headers)
