# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, current_app, jsonify, request

from dtop_machine.service.models import Project, db

bp = Blueprint('representation', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/representation/<int:study_id>', methods=['GET'])
def get_entity_representation(study_id: int):
    entity: Project = Project.query.filter_by(id=study_id).first()
    
    if entity is None:
        return jsonify({"status": "error", "message": "Entity not found."}), 404

    if not entity.status>=99:
        return jsonify({"status": "error", "message": "Cannot return the digital representation if the status of the entity is not larger than 99%"}), 404 

    try:
        dr = {
                "device": [],
                "prime_mover": []
            }
        
        if entity.type.lower() == "tec":
            type_tec = f"Horizontal {entity.model.model['number_rotor']}-Axis Turbine"
        else:
            type_tec = entity.model.model.get('machine_archetype', None)
            if type_tec is None:
                type_tec = "Wave Converter at Complexity 3"
            
        device = {
            "id": "",
            "location": [],
            "properties": {
                "technology": {
                    "description": "Identifies either a Tidal or Wave energy converter",
                    "value": entity.type,
                    "unit": "-",
                    "origin": "MC"
                },
                "type_of_technology": {
                    "description": "Specify the machine type, for the given Technology type",
                    "value": type_tec,
                    "unit": "-",
                    "origin": "MC"
                }
            }
        }
        
        prime_mover = {                
            "id": "",
            "location": [],
            "properties": {
                "technology": {
                    "description": "Identifies either a Tidal or Wave energy converter",
                    "value": entity.type,
                    "unit": "-",
                    "origin": "MC"
                },
                "type_of_technology": {
                    "description": "Specify the machine type, for the given Technology type",
                    "value": type_tec,
                    "unit": "-",
                    "origin": "MC"
                },
                "characteristic_length": {
                    "description": "Dimension of the device used for the energy absorption.",
                    "value": entity.dimensions.characteristic_dimension,
                    "unit": "m",
                    "origin": "MC"
                },
                "rated_capacity": {
                    "description": "Nominal rated capacity in kW of the machine.",
                    "value": entity.general.rated_capacity,
                    "unit": "kW",
                    "origin": "MC"
                }
            }
        }

        dr["device"].append(device)
        dr["prime_mover"].append(prime_mover)
        
        return jsonify(dr), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


