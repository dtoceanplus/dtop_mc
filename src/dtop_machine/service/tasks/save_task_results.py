# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json

import redis
from flask import current_app
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry

from dtop_machine.service import create_app
from dtop_machine.service.models import Task, db, Results, Project

import requests

def update_results(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        job = Queue().fetch_job(task_id)
        
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None
        # fetch the results of the BEM run.
        # those results have to be present otherwise the task will not run
        project = Project.query.filter_by(id=task.project_id).first()
        results = project.results
        results_json = results.results
        results_json.update(json.loads(job.result))
        results.results = results_json
        db.session.add(results)
        db.session.commit()

def save_results(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        job = Queue().fetch_job(task_id)
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None
        result = Results(project_id=task.project_id, results=json.loads(job.result))
        db.session.add(result)
        db.session.commit()

    # requests.post('http://127.0.0.1:5000/mc/1/single_machine_hydrodynamic/wec/complexity3', json=json.loads(job.result))