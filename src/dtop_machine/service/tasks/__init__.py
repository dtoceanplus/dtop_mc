# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import redis
import rq
import numpy as np
import json
import copy
from flask import Blueprint, current_app, jsonify, request

from dtop_machine import business
from dtop_machine.service.models import Model, Project, Task, db
from dtop_machine.service.tasks.save_task_results import save_results, update_results

from rq.command import send_stop_job_command

bp = Blueprint('tasks', __name__)
headers = {'Access-Control-Allow-Headers': 'Content-Type'}

@bp.route('/mc/<int:study_id>/task', methods=['GET'])
def get_task_status_from_project(study_id: int):
    task: Task = Task.query.filter_by(project_id=study_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)
        return jsonify({
            "task_id": task.id,
            "task_status": job.get_status(),
            "task_result": job.result,
            "worker_message": task.message,
            "worker_status": task.status,
            "job_tta": job.meta['tta']
        })
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@bp.route('/mc/tasks/<int:task_id>', methods=['GET'])
def get_project_task_status(task_id: int):
    task: Task = Task.query.filter_by(id=task_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)

        if job is None:
            return jsonify({
                "task_id": task.id,
                "task_status": 'deleted',
                "task_result": 'The Job associated with the task has been removed from the DataBase: this happens after 500s. Please run the task again or fetch the results from the Data Base',
            }), 200

        if job.get_status() == 'failed':
            raise RuntimeError(job.__dict__["exc_info"].split("raise")[-1])

        return jsonify({
            "task_id": task.id,
            "task_status": job.get_status(),
            "task_result": job.result,
        }), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


def fetch_data_from_db(prj):
    conversion_bem = {'force_tr_mat_real': 'force_transfer_matrix_real',
                      'force_tr_mat_imag': 'force_transfer_matrix_imag',
                      'diffraction_tr_mat_real': 'diffraction_transfer_matrix_real',
                      'diffraction_tr_mat_imag': 'diffraction_transfer_matrix_real',
                      'k_hst': 'hydrosatic_stiffness',
                      'm_m': 'mass_matrix',
                      'max_order': 'max_truncation_order',
                      'truncation_order': 'truncation_order',
                      'm_add': 'added_mass',
                      'modes': 'modes',
                      'order': 'order',
                      'c_rad': 'radiation_damping',
                      'cyl_radius': 'inscribing_cylinder_radius',
                      'f_ex_real': 'excitation_force_real',
                      'f_ex_imag': 'excitation_force_imag',
                      'amplitude_coefficient_radiation_real': 'amplitude_coefficient_radiation_real',
                      'amplitude_coefficient_radiation_imag': 'amplitude_coefficient_radiation_imag'}

    conversion_model = {'water_depth': 'water_depth',
                        'frequency': 'wave_frequency',
                        'directions': 'wave_direction',
                        'pto_dof': 'pto_dof',
                        'mooring_dof': 'mooring_dof'}

    conversion_performance = {'capture_width_ratio': 'capture_width_ratio',
                              'c_pto': 'pto_damping',
                              'heading_angle_span': 'heading_angle_span',
                              'directions': 'wave_angle_capture_width',
                              'tp': 'tp_capture_width',
                              'k_ext': 'additional_stiffness',
                              'te': 'tp_capture_width',
                              'k_mooring': 'mooring_stiffness',
                              'c_ext': 'additional_damping',
                              'spec_spreading': 'wave_spectra',
                              'hm0': 'hs_capture_width',
                              'spec_type': 'wave_spectra',
                              'spec_gamma': 'wave_spectra'}

    pm_data_raw = prj.results.results
    pm_data_bem = {k: pm_data_raw[v] for k, v in conversion_bem.items()}

    pm_data_model = prj.model.model
    model_data = {k: pm_data_model[v] for k, v in conversion_model.items()}
    model_data['periods'] = (1/np.array(model_data['frequency'])).tolist()
    pm_data_bem.update(model_data)

    pm_data_performance = {k: pm_data_model[v]
                           for k, v in conversion_performance.items()}


    # TODO: conver the capture width matrix to the power matrix
    pm_data_performance['power_matrix'] = (
        0*np.array(pm_data_performance['capture_width_ratio'])).tolist()
    pm_data_performance['spec_spreading'] = pm_data_performance['spec_spreading']['angular_spreading_factor']
    pm_data_performance['spec_type'] = pm_data_performance['spec_type']['spectrum_type']
    pm_data_performance['spec_gamma'] = pm_data_performance['spec_gamma']['peak_enhancement_factor']

    return pm_data_bem, pm_data_performance


@bp.route('/mc/tasks/<int:task_id>/performance', methods=['POST'])
def add_project_task_performance(task_id: int):
    try:
        task: Task = Task.query.filter_by(id=task_id).first()
        if task is None:
            return jsonify({"status": "error", "message": "Task not found."}), 400
        project_id = task.project_id
        project = Project.query.filter_by(
            id=project_id).outerjoin(Model).first()

        # results = project.results.results
        # model = project.model.model
        obj = business.MachineFactory.get_machine("WEC", 3)
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

    try:
        bem_inputs, performance_inputs = fetch_data_from_db(project)
        # fit_model = PowerMatrixFitting(bem_inputs, performance_inputs)
        # c_fit, k_fit, power_matrix = fit_model.performance_fitting()
        # pf_results = {"fitting_damping": c_fit,
        #               "fitting_stiffness": k_fit,
        #               "power_matrix": power_matrix}
        # results = dict(bem_inputs, **pf_results)

        # results = obj.power_performance_fit(bem_inputs, performance_inputs)

        # return jsonify({'message': f'valid project inputs', 'data': json.dumps( performance_inputs, cls=NumpyEncoder)}), 200
        # if not data_quality:
        #     raise IOError(f'The input data does not comply with the API format {bem_inputs["periods"]}')
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue()
            job = queue.enqueue(obj.power_performance_fit,
                                args=(bem_inputs, performance_inputs))
            queue.enqueue(update_results, job.get_id(), depends_on=job)
            task = Task(project_id=project.id, rq_job_id=job.get_id())
            db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({'message': str(e)}), 400

@bp.route('/mc/tasks', methods=['POST'])
def add_project_task():
    try:
        project_id = request.json["id"]
        project = Project.query.filter_by(
            id=project_id).outerjoin(Model).first()
        if project.type.upper() != 'WEC' or project.complexity != 3:
            raise IOError(
                'The redis worker is available only for Wave Converters at complexity 3')
        characteristic_dimension = project.dimensions.characteristic_dimension
        if characteristic_dimension <= 0:
            raise ValueError('Invalid Characterisitc Dimension saved into the DB')

        model = copy.deepcopy(project.model.model)
        model['characteristic_dimension'] = characteristic_dimension
        model['rated_power'] = project.general.rated_capacity*1000

        # only WEC at complexity 3 will be used
        obj = business.MachineFactory.get_machine("WEC", 3)
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

    try:
        data_quality = obj.verify_project_inputs(model)
        if not data_quality:
            raise IOError('The input data does not comply with the API format')
    except Exception as e:
        return jsonify({'message': f'invalid project inputs {str(e)}'}), 400

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue()
            job = queue.enqueue(obj.solve_bem_problem, args=(model,),
                                result_ttl=86400, job_timeout=86400)
            queue.enqueue(save_results, job.get_id(), depends_on=job)
            task = Task(project_id=project.id, rq_job_id=job.get_id())
            db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                    "DEBUG": model
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({'message': str(e)}), 400

@bp.route('/mc/<int:study_id>/task/kill', methods=['POST'])
def post_task_kill_from_project(study_id: int):
    task: Task = Task.query.filter_by(project_id=study_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        send_stop_job_command(redis.from_url(current_app.config["REDIS_URL"]),
            task.rq_job_id)

        return jsonify({
            "task_id": task.id,
            "task_status": 'aborted',
            "task_result": 'aborted',
            "worker_message": 'the actual execution has been aborted',
            "worker_status": 'failed',
            "job_tta": 0
        })
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500