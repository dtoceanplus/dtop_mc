# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, jsonify, make_response, request
import json

from dtop_machine.service.models import db, Project, Results
from dtop_machine.service.schemas import *
import logging

LOG = logging.getLogger()

bp = Blueprint('results', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

HYDRO_DB = './default_data/hydro_db.json'

def load_device_db(db_prj, param='point_absorber'):
    """load_device_db: load the data from the default 
    hydrodynamic DB stored in the ./default_data/hydro_db.json
    and returns the data accordying to the machine type or 
    archetype. The default case is set to point_absorber.

    return: mc hydrodynamic results structure
    """
    LOG.info('Loading the data from the default hydrodynamic database')
    hydro_db = json.load(open(HYDRO_DB, 'r'))
    if param is None:
        LOG.info('No request parameter is given, using the machine type or archetype')
        if db_prj.type.lower() == 'tec':
            return hydro_db['tidal']
        else:
            try:
                archetype = db_prj.model.model.get('machine_archetype', 'point_absorber')
            except:
                archetype = 'point_absorber'
            
    else:
        LOG.info(f'Request parameter is given. Return the hydrodynamic data for {param}')
        archetype = 'point_absrober'
        if param.lower() in hydro_db.keys():
            archetype = param.lower()

    return hydro_db[archetype]

@bp.route('/mc/<id>/single_machine_hydrodynamic/wec/complexity3', methods=['GET'])
@bp.route('/mc/<id>/single_machine_hydrodynamic', methods=['GET'])
def get_single_wec_complexity3(id):

    try:
        project = Project.query.filter_by(id=id).outerjoin('results').first()
        if project.complexity != 3 or project.type.lower() != 'wec':
            # TODO: return the data based on the machine archetipe/shape
            # Check for query param (i.e. ?archetype='point_absorber')
            # TODO: this functionality is undocumented 
            query_parameter = request.args.get('archetype', None)
            db_results = load_device_db(project, param=query_parameter)
            return make_response(jsonify(db_results),
                             200,
                             headers)
        else:
            results = project.results

            if project.results is None:
                raise IOError(f'No model model available for project {id}')

            return make_response(jsonify(results.results),
                             200,
                             headers)
    except Exception as e:
        return make_response(jsonify({"status": "error", "message": str(e)}),
                             404,
                             headers)


# undocumented route
@bp.route('/mc/<id>/single_machine_hydrodynamic/wec/complexity3', methods=['POST'])
@bp.route('/mc/<id>/single_machine_hydrodynamic', methods=['POST'])
def post_single_wec_complexity3(id):
    try:
        request_body = request.get_json()
        result = Results(project_id=id, results=request_body)
        
        db.session.add(result)  # Adds new User record to database
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': f'Results added to the project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': 'Error while saving the results'}),
                             400,
                             headers)
