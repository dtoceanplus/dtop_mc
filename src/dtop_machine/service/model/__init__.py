# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, make_response, url_for, request, jsonify

from dtop_machine.service.models import *
from dtop_machine.service.schemas import *

bp = Blueprint('model', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/mc/<id>/model/wec/complexity1', methods=['PUT'])
def put_wec_complexity1(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
                             400,
                             headers)

def estimate_pto(archetype, dimensions):
    """Estimate the pto damping at complexity 1 based on the 
    archetype of the wec and its mass
    """
    reference_data = {
        'point_absorber': {
            'mass': 805033, 
            'pto': 793000
        },
        'terminator': {
            'mass': 666250, 
            'pto': 168000000
        },
        'attenuator': {
            'mass': 1607748, 
            'pto': 211000000
        }
    }
    if dimensions == None: 
        return -1
    else:
        mass = dimensions.mass

    if archetype == "point_absorber":
        return reference_data['point_absorber']['pto']*mass/reference_data['point_absorber']['mass']
    elif archetype == "terminator":
        return reference_data['terminator']['pto']*mass/reference_data['terminator']['mass']
    elif archetype == "attenuator":
        return reference_data['attenuator']['pto']*mass/reference_data['attenuator']['mass']
    else:
        return -1

@bp.route('/mc/<id>/model/wec/complexity1', methods=['GET'])
def get_wec_complexity1(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model
        dimensions = project.dimensions 

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        pto_damping = estimate_pto(model.model['machine_archetype'], dimensions)
        model_json = jsonify({**model.model, **{"pto_damping": [pto_damping]}})

        return make_response(model_json,
                             200,
                             headers)
    except Exception as e:
        
        return make_response(jsonify({'message': f'A problem occured for the model input of project {id}: {str(e)}'}),
                             404,
                             headers)

@bp.route('/mc/<id>/model/wec/complexity2', methods=['PUT'])
def put_wec_complexity2(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
                             400,
                             headers)

@bp.route('/mc/<id>/model/wec/complexity2', methods=['GET'])
def get_wec_complexity2(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        model.model['pto_damping'] = [model.model['pto_damping']]

        return make_response(jsonify(model.model),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot find the model input for project {id}: missing ID'}),
                             404,
                             headers)

@bp.route('/mc/<id>/model/wec/complexity3', methods=['PUT'])
def put_wec_complexity3(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except Exception as e:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}: {str(e)}'}),
                             400,
                             headers)

@bp.route('/mc/<id>/model/wec/complexity3', methods=['GET'])
def get_wec_complexity3(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        return make_response(jsonify(model.model),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot find the model input for project {id}: missing ID'}),
                             404,
                             headers) 

@bp.route('/mc/<id>/model/tec/complexity1', methods=['PUT'])
def put_tec_complexity1(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
                             400,
                             headers)

@bp.route('/mc/<id>/model/tec/complexity1', methods=['GET'])
def get_tec_complexity1(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        return make_response(jsonify(model.model),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot find the model input for project {id}: missing ID'}),
                             404,
                             headers)

@bp.route('/mc/<id>/model/tec/complexity2', methods=['PUT'])
def put_tec_complexity2(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
                             400,
                             headers)

@bp.route('/mc/<id>/model/tec/complexity2', methods=['GET'])
def get_tec_complexity2(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        return make_response(jsonify(model.model),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot find the model input for project {id}: missing ID'}),
                             404,
                             headers)

@bp.route('/mc/<id>/model/tec/complexity3', methods=['PUT'])
def put_tec_complexity3(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if project.model is None:
            model = Model(model=request.get_json(), project=project)
        else:  
            model = project.model
            model.update(model=request.get_json())
                    
        db.session.add(model)
        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
                             400,
                             headers)

@bp.route('/mc/<id>/model/tec/complexity3', methods=['GET'])
def get_tec_complexity3(id):

    try:
        project = Project.query.filter_by(id=id).first()
        model = project.model

        if project.model is None:
            raise IOError(f'No model model available for project {id}')

        return make_response(jsonify(model.model),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot find the model input for project {id}: missing ID'}),
                             404,
                             headers)                             
