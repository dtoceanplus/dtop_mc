# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, make_response, url_for, request, jsonify

from dtop_machine.service.models import *
from dtop_machine.service.schemas import *

bp = Blueprint('dimensions', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/mc/<id>/dimensions', methods=['PUT'])
def save_dimensions(id):

    try:
        project = Project.query.filter_by(id=id).first()
        if project.dimensions is None:
            dimensions = Dimensions(**request.get_json(), project=project)
        else:  
            dimensions = project.dimensions
            body = request.get_json()
            dimensions.update(**request.get_json())
                    
        db.session.add(dimensions)
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': f'Dimensions inputs added for project {id}'}),
                             201,
                             headers)
    except Exception as e:
        return make_response(jsonify({'message': str(e)}),
                             400,
                             headers)

@bp.route('/mc/<id>/dimensions', methods=['GET'])
def get_dimensions(id):
    try:
        project = Project.query.filter_by(id=id).first()
        dimensions = project.dimensions 
        if dimensions is None:
            raise RuntimeError('Dimension inputs not saved to the DB')
        return make_response(jsonify(dimensions_schema.dump(dimensions)),
                             200,
                             headers)
    except Exception as e:
        
        return make_response(jsonify({'message': f'Cannot return the dimension input for project {id}: {str(e)}'}),
                             404,
                             headers)
