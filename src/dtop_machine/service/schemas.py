# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from .models import Project, Dimensions, Model, General, Results

from flask_marshmallow import Marshmallow
from marshmallow import post_dump
import ast
import json
ma = Marshmallow()

class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Project
        include_relationships = True

projects_schema = ProjectSchema(many=True)
project_schema = ProjectSchema()

class ModelSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Model

model_schema = ModelSchema()

class ResultsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Results

results_schema = ResultsSchema()

class GeneralSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = General

    @post_dump(pass_many=False)
    def modify_out(self, data, many):
        # TODO: remove this patch
        data['materials'] = eval(data['materials'])
        return data    

general_schema = GeneralSchema()

class DimensionsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Dimensions

    @post_dump(pass_many=False)
    def modify_out(self, data, many):
        # TODO: remove this patch
        data.pop('project', None)
        return data

dimensions_schema = DimensionsSchema()


def validate_numerical_model_schema(model, cpx, mac):
    schema = schemas[mac.lower()][int(cpx)-1]
    error_str_under = ""
    error_str_over = ""
    model_under = list(set(schema)-set(model))
    model_over = list(set(model)-set(schema))
    
    if model_under:
        keys = ', '.join(model_under)
        error_str_under = f'The provided schema is missing the following keys: {keys}'
    if model_over:
        keys = ', '.join(model_over)
        error_str_over = f'The following keys should not be provided for the schema: {keys}'
    if model_over or model_under:
        raise ValueError(f'{error_str_under} {error_str_over}')

schemas = {}
schemas['tec'] = [
    {
        "cp": 0.3,
        "number_rotor": 1
    },
    {
        "tip_speed_ratio": 1,
        "cp": 0.3,
        "ct": 0.4,
        "cut_in_velocity": 1,
        "cut_out_velocity": 10,
        "number_rotor": 1,
        "rotor_interdistance": 0
    },
    {  
        "tip_speed_ratio": 1,
        "cp": [0, 0.3, 1, 1, 0.5, 0],
        "ct": [0, 0.3, 0.3, 0.3, 0.5, 0],
        "cp_ct_velocity": [0, 0.5, 1, 2, 5, 10],
        "cut_in_velocity": 1,
        "cut_out_velocity": 10,
        "heading_angle_span": 0,
        "bidirectional": True,
        "number_rotor": 1,
        "rotor_interdistance": 0
    }
]

schemas['wec'] = [
    {
        "capture_width_ratio": 0.3,
        "machine_archetype": "Point Absorber"
    },
    {
        "capture_width_ratio": [[[0.3]]],
        "tp_capture_width": [5],
        "hs_capture_width": [2],
        "wave_angle_capture_width": [0],
        "pto_damping": 1e6,
        "mooring_stiffness": 2e6,
        "machine_archetype": "Point Absorber"
    },
    {
        "capture_width_ratio": [[[0.3]]],
        "tp_capture_width": [5],
        "hs_capture_width": [2],
        "wave_angle_capture_width": [0],
        "pto_damping": [[[1e6]]],
        "mooring_stiffness": [[[2e6]]],
        "additional_damping": [[[1e6]]],
        "additional_stiffness": [[[2e6]]],
        "wave_spectra": {
            "spectrum_type": "JONSWAP",
            "peak_enhancement_factor": 3.3,
            "angular_spreading_factor": 3
        },
        "heading_angle_span": 30,
        "water_depth": 50,
        "shared_dof": [0,0,1,0,0,0],
        "point_application": [0,0,0.0],
        "wave_frequency": [0.1],
        "wave_direction": [0],
        "get_array_mat": True,
        "dofs": ["Heave"],
        "cyl_theta": 11,
        "cyl_zeta": 15,
        "ndof": 1,
        "pto_dof": 1,
        "mooring_dof": 1,
        "bodies": [
            {
                "ID": [0],
                "axis_angles": [0,0,0],
                "child_dof_position": [],
                "cog": [0,0,0],
                "dof_with_parent": [],
                "extended_dof": 0,
                "MoI": [[1,0,0],[0,1,0],[0,0,1]],
                "mass": 1e6,
                "mesh": "./test/business/data/Cylinder.dat",
                "mesh_format": "dat",
                "number_panel": 500,
                "number_vertex": 2000,
                "number_child": 0,
                "parent_body": [-1]
            }
        ]
    }
]
