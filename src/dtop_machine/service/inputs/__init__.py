# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, make_response, url_for, request, jsonify

from dtop_machine.service.models import *
from dtop_machine.service.schemas import *
import logging

LOG = logging.getLogger(__name__)

bp = Blueprint('inputs', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

# TODO: control the numerical_model input schema for the different machine and complexity level 
@bp.route('/mc/<id>/inputs/upload/tec/complexity1', methods=['POST'])
@bp.route('/mc/<id>/inputs/upload/tec/complexity2', methods=['POST'])
@bp.route('/mc/<id>/inputs/upload/tec/complexity3', methods=['POST'])
@bp.route('/mc/<id>/inputs/upload/wec/complexity1', methods=['POST'])
@bp.route('/mc/<id>/inputs/upload/wec/complexity2', methods=['POST'])
@bp.route('/mc/<id>/inputs/upload/wec/complexity3', methods=['POST'])
def post_upload_inputs(id):
    try:
        binary_json = request.files['file'].read()
        my_json = binary_json.decode('utf-8')
        data = json.loads(my_json)
    except:
        return jsonify({'message': 'File Upload Fail'}), 400

    try:
        project = Project.query.filter_by(id=id).first()
        if project is None:
            return make_response(jsonify({'message': f'Invalid project ID: {id}'}),
                                400,
                                headers)
        numerical_model = data.pop('numerical_model')
        dimensions_data = data.pop('dimensions')

        # check if the data exist before forcing into the system
        if numerical_model is not None:
            validate_numerical_model_schema(numerical_model, 
                                        project.complexity,
                                        project.type)                        
            if project.model is None:
                model = Model(model=numerical_model, project=project)
            else:  
                model = project.model
                model.update(model=numerical_model)
            db.session.add(model)

        # check if the data exist before forcing into the system
        if dimensions_data is not None:
            if project.dimensions is None:
                dimensions = Dimensions(**dimensions_data, project=project)
            else:  
                dimensions = project.dimensions
                dimensions.update(**dimensions_data)
            db.session.add(dimensions)
        

        materials = data.pop('materials', None)
        if materials is not None:
            if project.general is None:
                general = General(**data, project=project)
            else:  
                general = project.general
                general.update(**data)
            general.materials = json.dumps(materials)    
            db.session.add(general)

        db.session.commit()  # Commits all changes

        return make_response(jsonify({'message': f'Model added to project {id}'}),
                             201,
                             headers)
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400 

# @bp.route('/mc/<id>/inputs/upload/tec/complexity2', methods=['POST'])
# @bp.route('/mc/<id>/inputs/upload/tec/complexity3', methods=['POST'])
# @bp.route('/mc/<id>/inputs/upload/wec/complexity1', methods=['POST'])
# @bp.route('/mc/<id>/inputs/upload/wec/complexity2', methods=['POST'])
# @bp.route('/mc/<id>/inputs/upload/wec/complexity3', methods=['POST'])
# def put_inputs_complexity(id):
#     try:
#         project = Project.query.filter_by(id=id).first()
#         if project is None:
#             return make_response(jsonify({'message': f'Invalid project ID: {id}'}),
#                                 400,
#                                 headers)

#         body = request.get_json()
#         numerical_model = body.pop('numerical_model')
#         dimensions_data = body.pop('dimensions')
        
#         if project.model is None:
#             model = Model(model=numerical_model, project=project)
#         else:  
#             model = project.model
#             model.update(model=numerical_model)

#         if project.dimensions is None:
#             dimensions = Dimensions(**dimensions_data, project=project)
#         else:  
#             dimensions = project.dimensions
#             dimensions.update(**dimensions_data)  

#         if project.general is None:
#             general = General(**body, project=project)
#         else:  
#             general = project.general
#             general.update(**body)      
                    
#         db.session.add(model)
#         db.session.add(dimensions)
#         db.session.add(general)

#         db.session.commit()  # Commits all changes

#         return make_response(jsonify({'message': f'Model added to project {id}'}),
#                              201,
#                              headers)
#     except:
        
#         return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
#                              400,
#                              headers)

# @bp.route('/mc/<id>/inputs/upload/tec/complexity2', methods=['POST'])
# def put_inputs_tec_complexity3(id):
#     try:
#         project = Project.query.filter_by(id=id).first()
#         if project is None:
#             return make_response(jsonify({'message': f'Invalid project ID: {id}'}),
#                                 400,
#                                 headers)

#         body = request.get_json()
#         numerical_model = body.pop('numerical_model', None)
#         dimensions_data = body.pop('dimensions', None)
        
#         if project.model is None:
#             model = Model(model=numerical_model, project=project)
#         else:  
#             model = project.model
#             model.update(model=numerical_model)

#         if project.dimensions is None:
#             dimensions = Dimensions(**dimensions_data, project=project)
#         else:  
#             dimensions = project.dimensions
#             dimensions.update(**dimensions_data)  

#         if project.general is None:
#             general = General(**body, project=project)
#         else:  
#             general = project.general
#             general.update(**body)      
                    
#         db.session.add(model)
#         db.session.add(dimensions)
#         db.session.add(general)

#         db.session.commit()  # Commits all changes

#         return make_response(jsonify({'message': f'Model added to project {id}'}),
#                              201,
#                              headers)
#     except:
        
#         return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
#                              400,
#                              headers)   

# @bp.route('/mc/<id>/inputs/upload/tec/complexity3', methods=['POST'])
# def put_inputs_complexity3(id):
#     try:
#         project = Project.query.filter_by(id=id).first()
#         if project is None:
#             return make_response(jsonify({'message': f'Invalid project ID: {id}'}),
#                                 400,
#                                 headers)

#         body = request.get_json()
#         numerical_model = body.pop('numerical_model', None)
#         dimensions_data = body.pop('dimensions', None)
        
#         if project.model is None:
#             model = Model(model=numerical_model, project=project)
#         else:  
#             model = project.model
#             model.update(model=numerical_model)

#         if project.dimensions is None:
#             dimensions = Dimensions(**dimensions_data, project=project)
#         else:  
#             dimensions = project.dimensions
#             dimensions.update(**dimensions_data)  

#         if project.general is None:
#             general = General(**body, project=project)
#         else:  
#             general = project.general
#             general.update(**body)      
                    
#         db.session.add(model)
#         db.session.add(dimensions)
#         db.session.add(general)

#         db.session.commit()  # Commits all changes

#         return make_response(jsonify({'message': f'Model added to project {id}'}),
#                              201,
#                              headers)
#     except:
        
#         return make_response(jsonify({'message': f'Cannot add model to project {id}'}),
#                              400,
#                              headers)                                                       
