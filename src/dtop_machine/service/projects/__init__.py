# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, make_response, url_for, request, jsonify

from dtop_machine.service import config

from dtop_machine.service.models import *
from dtop_machine.service.schemas import project_schema, projects_schema
import json

bp = Blueprint('projects', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

DEFAULT_DB = './default_data/default_projects.json'

@bp.route('/mc/logs', methods=['GET'])
def get_logs():
    try:
        # Extract the Log File
        logfile = open("./logs/mc.log","r") 

        # Return the file and the date
        return logfile.read(),200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

@bp.route('/mc/<id>', methods=['PUT'])
def edit_project(id):
    try:
        project = Project.query.filter_by(id=id).first()
        if not project:
            raise IOError('Invalid project ID')

        request_body = request.get_json()
        # request_body['complexity'] = int(request_body['complexity'])
    
        project.update(**request_body)  # Create an instance of the User class
        db.session.add(project)  # Adds new User record to database
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': 'Project modified'}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': 'Error while parsing the projects'}),
                             400,
                             headers)

@bp.route('/mc/<id>', methods=['DELETE'])
def remove_project(id):
    try:
        project = Project.query.filter_by(id=id).first()
        if not project:
            raise IOError('Invalid project ID')
    
        db.session.delete(project)
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': f'Project {id} removed'}),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot remove project {id}: missing ID'}),
                             404,
                             headers)

@bp.route('/mc/<id>', methods=['GET'])
def fetch_project(id):
    
    try:
        project = Project.query.filter_by(id=id).first()
        if not project:
            raise IOError('Invalid project ID')
        return make_response(jsonify(project_schema.dump(project)),
                             200,
                             headers)
    except:
        
        return make_response(jsonify({'message': f'Cannot remove project {id}: missing ID'}),
                             404,
                             headers)

@bp.route('/mc', methods=['GET'])
def fetch_all():
    try:
        all_projects = Project.query.all()
        if not all_projects:
            raise IOError('No Machine Project saved into the DB')
        return make_response(jsonify(projects_schema.dump(all_projects)), 200, headers)
    except Exception as e:
        return make_response(jsonify({'message': str(e)}),
                             404,
                             headers)

@bp.route('/mc', methods=['POST'])
def add_project():
    
    try:
        request_body = request.get_json()
        request_body['complexity'] = int(request_body['complexity'])

        # TODO: understand if this is needed or not
        # existing_project = Project.query.filter(Project.title == request_body['title']).first()
        # if existing_project:
        #     return jsonify({'message': (f'Project title {existing_project.title} '
        #                                 f'already exist. The project title is a unique key of the DB'
        #                                 f', change it and resubmit the form')}), 400


        new_project = Project()
        new_project.update(**request_body)  # Create an instance of the User class
        db.session.add(new_project)  # Adds new User record to database
        db.session.commit()  # Commits all changes
        return make_response(jsonify({'message': 'Project added', 'ID': new_project.id}),
                             201,
                             headers)
    except:
        
        return make_response(jsonify({'message': 'Error while parsing the projects'}),
                             400,
                             headers)

@bp.route('/mc/<int:mc_id>/clear', methods=['DELETE'])
def remove_project_results(mc_id: int):
    project: Project = Project.query.get(mc_id)
    tasks: Task = Task.query.filter_by(project_id=mc_id)

    if project is None:
        return jsonify({"status": "error", "message": "Project not found."}), 404
    results = project.results
    try:
        if results:
            db.session.delete(results)
    except:
        return jsonify({"status": "error", "message": "Cannot delete project results"}), 404

    try:
        if tasks:
            for task in tasks:
                db.session.delete(task)
    except:
        return jsonify({"status": "error", "message": "Cannot delete project tasks"}), 404

    try:
        db.session.commit()
        return jsonify({"status": "success", 'message': 'Project results and task deleted'}), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404

@bp.route('/mc/default/db', methods=['GET'])
def fetch_all_default():
    try:
        projects_list = json.load(open(DEFAULT_DB, 'r'))
        for el in projects_list:
            el.pop('general')
            el.pop('dimensions')
            el.pop('model')

        return make_response(jsonify(projects_list), 200, headers)
    except Exception as e:
        return make_response(jsonify({'message': str(e)}),
                             404,
                             headers)

@bp.route('/mc/<mc_id>/default/db/<db_id>', methods=['POST'])
def fetch_default_id(mc_id, db_id):
    try:
        project = Project.query.filter_by(id=mc_id).first()
        if project is None:
            raise ValueError(f'Invalid MC project ID: {mc_id}')

        projects_list = json.load(open(DEFAULT_DB, 'r'))
        default_data = [el for el in projects_list if el['ID'] == int(db_id)]

        if not default_data:
            raise ValueError(f'Provided ID for the default DB not found in the Default DataBase. Provided ID: {db_id}')
        
        default_data = default_data[0]
        default_data.pop('ID', None)
        # save general data
        general_data = default_data['general']
        general_data.pop('id')
        materials = general_data.pop('materials')
        general = General(**general_data, project=project)
        general.materials = json.dumps(materials)

        # save dimensions
        dimensions_data = default_data['dimensions']
        dimensions_data.pop('id')
        dimensions = Dimensions(**dimensions_data, project=project)

        # save model
        model_data = default_data['model']
        model = Model(model=model_data, project=project)

        if project.type.lower() == 'wec' and project.complexity == 3:
            # save also the results
            hydro_data = default_data['hydro']
            result = Results(results=hydro_data, project=project)
            db.session.add(result)
        # save and close the DB
        db.session.add(general)
        db.session.add(dimensions)
        db.session.add(model)
        db.session.commit()  # Commits all changes


        return make_response(jsonify({'message': f'Default data correctly saved to the MC entity {mc_id}'}), 200, headers)
    except Exception as e:
        return make_response(jsonify({'message': str(e)}),
                             404,
                             headers)

# # TODO: REMOVE!!! temporary rooute to add data to the DB
# @bp.route('/mc_add', methods=['GET'])
# def add_all():
#     result = []
#     try:
#         for field in config.PROJECTS:
#             temp = ({
#                             "title": field['title'],
#                             "desc": field['desc'],
#                             "complexity": int(field['complexity']),
#                             "type": field['type'].upper(),
#                             "date": field['date'],
#                             "status": field['status'],
#                             "tags": field['tags'],
#                             "farm": f"/ec/{field['id']}/farm",
#                             "devices": f"/ec/{field['id']}/devices"
#                         })
#             result.append(temp)
#             existing_project = Project.query.filter(Project.title == field['title'] or
#                                                     Project.farm == f"/ec/{field['id']}/farm" or
#                                                     Project.devices == f"/ec/{field['id']}/devices").first()

#             if existing_project:
#                 return jsonify({'error_message': (f'Project title {existing_project.title} '
#                                                 f'already exist. The project title is a unique key of the DB'
#                                                 f', change it and resubmit the form')}), 400

#             new_project = Project(**temp)  # Create an instance of the User class
#             db.session.add(new_project)  # Adds new User record to database
#             db.session.commit()  # Commits all changes
    
#         return jsonify(result), 200
#     except:
#         return jsonify({'error_message': 'error while parsing the projects'}), 400

