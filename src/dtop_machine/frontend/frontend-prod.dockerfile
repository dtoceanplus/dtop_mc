FROM node:14.15-alpine as build-stage

WORKDIR /app
COPY ./frontend .

RUN npm ci
RUN npm run build:prod

FROM nginx:1.19-alpine

COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/prod.conf /etc/nginx/conf.d/local.conf

# for the build context -  ./src/dtop_mc
COPY ./frontend/nginx.conf /etc/nginx/nginx.conf
COPY ./service/static/openapi.json /usr/share/nginx/html/static/openapi.json

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
