FROM node:8.16-alpine

RUN apk add --no-cache make

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY *rc ./
COPY *.js ./
COPY Makefile .

CMD make x-frontend-run
