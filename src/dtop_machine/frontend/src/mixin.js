// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import axios from 'axios'
import { reshape, range, squeeze, diag, max, min, zeros } from 'mathjs'

export default {
  data() {
    'Hello World'
    null
  },
  created: function() {
    console.log('Printing from the Mixin tested')
  },
  computed: {
    studyId() {
      return this.$store.getters.studyId
    }
  },
  methods: {
    bodyMesh(mesh_txt) {
      var data_mesh = []
      for (let b_ind = 0; b_ind < mesh_txt.length; b_ind++) {
        debugger
        var b1_m = mesh_txt[b_ind].mesh_raw
        var array = []
        var m = { name: mesh_txt[b_ind].mesh.slice(0,-4), 
                  type: 'mesh3d', x: [], y: [], z: [], i: [], j: [], k: [],
                  showlegend: true}
        var i = 1
        while (true) { // for (var i=1; i<3000; i++) {
          // array = b1_m[i].match(/\d+(?:\.\d+)?/g).map(Number)
          array = b1_m[i].split(/\r?\n|[ ]+/).filter(Boolean).map(Number)
          // array = b1_m[i].split(' ').map(Number)
          if (array[0] == 0) {
            break
          }
          m.x.push(array[1])
          m.y.push(array[2])
          m.z.push(array[3])
          i++
        }
        for (var j = i + 1; j < b1_m.length; j++) {
          array = b1_m[j].match(/\d+(?:\.\d+)?/g).map(Number)
          if (array[0] == 0) {
            break
          }
          m.i.push(array[0] - 1)
          m.j.push(array[1] - 1)
          m.k.push(array[2] - 1)
          if (array.length == 4) {
            m.i.push(array[0] - 1)
            m.j.push(array[2] - 1)
            m.k.push(array[3] - 1)
          }
        }
        data_mesh.push(m)
      }
      return data_mesh
    },
    unravel(I, X, Y, Z, x, y, z) {
      var arr = zeros(x.length, y.length, z.length)._data
      var l = 0
      var m = 0
      var n = 0
      for (let i=0; i<I.length; i++) {
        l = x.indexOf(X[i])
        m = y.indexOf(Y[i])
        n = z.indexOf(Z[i])
        arr[l][m][n] = I[i]
      }
      return arr
    },
    cwrMatrixHandling() {
      debugger
      var hs = this.tableData.map( ob => ob.Hs )
      var tp = this.tableData.map( ob => ob.Tp )
      var dir = this.tableData.map( ob => ob.Dir )
      var cwr = this.tableData.map( ob => ob.CWR )

      const hs_u = [...new Set(hs)]
      const tp_u = [...new Set(tp)]
      const dir_u = [...new Set(dir)]

      if (hs_u.length*tp_u.length*dir_u.length !== hs.length) {
        if (hs_u.length === hs.length && tp_u.length === tp.length && dir_u.length === 1) {
          // 1D data provided
          var hs_n = []
          var tp_n = []
          var dir_n = []
          var cwr_n = []
          for (let ihs=0; ihs<hs_u.length; ihs++) {
            for (let itp=0; itp<tp_u.length; itp++) {
              for (let idir=0; idir<dir_u.length; idir++) {
                hs_n.push(hs_u[ihs])
                tp_n.push(tp_u[itp])
                dir_n.push(dir_u[idir])
                cwr_n.push(cwr[itp])
              }
            }
          }
          hs = hs_n
          tp = tp_n
          dir = dir_n
          cwr = cwr_n
          debugger
        } else {
          this.$notify({
            title: 'Invalid Data Format',
            message: 'The data uploaded contains repeated elements for the same triplet (Hs, Tp, Dir). Please modify the data and upload the new file.',
            type: 'error'
          })
          this.tableData = []
          this.tableHeader = []
          this.cwr = { tp: [], hs: [], dir: [], cwr: [] }
          this.tp = []
          this.hs = []
          this.beta = []
          this.cwr_mat = []
          this.directions = []
          return -1
        }
      }
      // save the flattened data into the CWR object
      // this is the shape to be saved into the DB
      this.cwr.hs = hs
      this.cwr.tp = tp
      this.cwr.dir = dir
      this.cwr.cwr = cwr

      // format the data for the Contour plot
      this.tp = tp_u
      this.hs = hs_u
      this.beta = [...new Set(dir)]
      this.cwr_mat = this.unravel(cwr, tp, hs, dir, this.tp, this.hs, this.beta)
      this.directions = this.beta.reduce((o, el, i) => ({ ...o, [i]: el + ' deg' }), {})
    },
    displayMessage: function() {
      console.log('Now printing from a mixin function')
    },
    deleteInputs(mcId) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${mcId}/inputs`;
      return axios.delete(path)
    },
    deleteDevices(mcId) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${mcId}/devices`;
      return axios.delete(path)
    },
    deleteFarm(mcId) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${mcId}/farm`;
      return axios.delete(path)
    },
    fetch_project(id) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${id}`

      axios
        .get(path)
        .then(res => {
          this.complexity = res.data.complexity
          this.machine = res.data.type
        })
        .catch(error => {
          console.error(error)
        })
    },
    fetch_inputs_model(id, complexity, machine) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${id}/model/${machine}/complexity${complexity}`
      axios
        .get(path)
        .then(res => {
          this.form = res.data
        })
        .catch(error => {
          console.error(error)
        })
    },
    save_inputs_model(id, complexity, machine, payload) {
      const path = `${process.env.VUE_APP_API_URL}/mc/${id}/model/${machine}/complexity${complexity}`
      console.log('SAVE MODLE', payload)
      axios
        .put(path, payload)
        .then(res => {
          this.$message({
            message: 'Inputs saved',
            type: 'success'
          })
          this.$store.dispatch('studies/updateStatus', { key: 'model' , value: true })
        })
        .catch(error => {
          console.error(error)
          this.$message({
            message: error,
            type: 'error' })
        })
    }
  }
}
