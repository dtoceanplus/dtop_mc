// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import LayoutNoSide from '@/layout/LayoutNoSide.vue'

export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Home', icon: 'dashboard' }
    },
    {
      path: 'logs',
      name: 'ECLogs',
      component: () => import('@/views/logs/index'),
      meta: { title: 'Logs', icon: 'logs' }
    },
    {
      path: 'defualt_db',
      name: 'DefaultDB',
      component: () => import('@/views/default_db/index'),
      meta: { title: 'Default DB', icon: 'defualt_db' }
    }]
  },
  {
    path: '/mc-entity',
    name: 'MCEntity',
    component: () => import('@/views/mc_entity/index'),
    meta: { title: 'Integration' },
    hidden: true
  },
  {
    path: '/mc-studies',
    component: Layout,
    name: 'MCStudies',
    meta: { title: 'Studies', icon: 'table' },
    children: [
      {
        path: 'index',
        name: 'MCStudiesIndex',
        component: () => import('@/views/mc_studies/index'),
        meta: { title: 'List' },
        hidden: true
      },
      {
        path: 'inputs',
        name: 'MCStudyInput',
        component: () => import('@/views/mc_study/index'),
        meta: { title: 'Inputs' },
        children: [{
          path: 'general',
          name: 'GeneralInput',
          component: () => import('@/views/mc_study/general/index'),
          meta: { title: 'General' }
        },
        {
          path: 'dimension',
          name: 'DimensionInput',
          component: () => import('@/views/mc_study/dimension/index'),
          meta: { title: 'Dimensions' }
        },
        {
          path: 'model',
          name: 'ModelInput',
          component: () => import('@/views/mc_study/model/index'),
          meta: { title: 'Model' }
        },
        {
          path: 'hydro',
          name: 'HydroOutput',
          component: () => import('@/views/mc_study/output/index'),
          meta: { title: 'HydroOutput' },
          hidden: true
        }
        ]
      },
      {
        path: 'outputs',
        name: 'MCStudyOutput',
        component: () => import('@/views/mc_study/output/index'),
        meta: { title: 'Output' },
        hidden: true
      }
    ]
  },

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },

  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },

  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },

  {
    path: 'external-link',
    component: LayoutNoSide,
    name: 'Links',
    meta: {
      title: 'Links',
      icon: 'link'
    },
    children: [
      {
        path: 'https://www.dtoceanplus.eu/',
        meta: { title: 'DTOceanPlus' }
      },
      {
        path: 'http://127.0.0.1:5000/static/docs/index.html',
        meta: { title: 'Sphinx' }
      },
      {
        path: 'http://127.0.0.1:5000/api/',
        meta: { title: 'ReDoc (API)' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
