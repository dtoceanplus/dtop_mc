// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  studyId: state => state.studies.studyId,
  characteristicLength: state => state.studies.characteristicLength,
  studyTitle: state => state.studies.studyTitle,
  wecBodies: state => state.studies.wecBodies,
  wecBodiesIDs: state => state.studies.wecBodiesIDs,
  wecJoints: state => state.studies.wecJoints,
  completion_state: state => state.studies.completion_state,
  status: state => state.studies.status,
  complexity: state => state.studies.complexity,
  machine_type: state => state.studies.machine_type,
}
export default getters
