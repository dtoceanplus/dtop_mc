// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import axios from 'axios'

const state = {
  studyId: null,
  studyTitle: null,
  wecBodies: [],
  wecJoints: [],
  wecBodiesIDs: [],
  completion_state: { general: false, dimensions: false, model: false, results: false, lock: false },
  status: 0,
  complexity: 1,
  machine_type: 'tec',
  characteristicLength: 1
}

const mutations = {
  SET_ID: (state, studyId) => {
    state.studyId = studyId
  },
  SET_TITLE: (state, studyTitle) => {
    state.studyTitle = studyTitle
  },
  SET_CHAR_DIM: (state, characteristicLength) => {
    state.characteristicLength = characteristicLength
  },
  SET_BODIES: (state, wecBodies) => {
    state.wecBodies = wecBodies
    if (wecBodies.length>0) {
      state.wecBodiesIDs = wecBodies.map(o=>o.ID[0])
    }
  },
  SET_JOINTS: (state, wecJoints) => {
    state.wecJoints = wecJoints
  },
  UPDATE_STATUS: (state, {key, value}) => {
    state.completion_state[key] = value;
    var status = 0;
    if (state.complexity === 3 && state.machine_type === 'wec') {
      status += state.completion_state['general']*18;
      status += state.completion_state['dimensions']*18;
      status += state.completion_state['model']*18;
      status += state.completion_state['results']*45;
      status += state.completion_state['lock']*1;
    } else {
      status += state.completion_state['general']*33;
      status += state.completion_state['dimensions']*33;
      status += state.completion_state['model']*33;
      status += state.completion_state['lock']*1;
    }
    state.status = status
    const path = `${process.env.VUE_APP_API_URL}/mc/${state.studyId}`
    axios.put(path, { status: state.status, data_lock: state.completion_state['lock']})
      .then(response => {
        console.log('STATUS UPDATED', { complexity: state.complexity, status: state.status })
      }
    )
  },
  SET_TYPE: (state, machine) => {
    state.machine_type = machine.toLowerCase()
  },
  SET_COMPLEXITY: (state, complexity) => {
    state.complexity = complexity
  }
}

const actions = {
  setId({ commit }, { studyId }) {
    commit('SET_ID', studyId)
  },
  setTitle({ commit }, { studyTitle }) {
    commit('SET_TITLE', studyTitle)
  },
  setCharDim({ commit }, { characteristicLength }) {
    commit('SET_CHAR_DIM', characteristicLength)
  },
  setBodies({ commit }, { wecBodies }) {
    commit('SET_BODIES', wecBodies)
  },
  setJoints({ commit }, { wecJoints }) {
    commit('SET_JOINTS', wecJoints)
  },
  updateStatus({ commit }, { key, value }) {
    commit('UPDATE_STATUS', { key: key, value: value })
  },
  setMachine({ commit }, { machine }) {
    commit('SET_TYPE', machine)
  },
  setComplexity({ commit }, { complexity }) {
    commit('SET_COMPLEXITY', complexity)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
