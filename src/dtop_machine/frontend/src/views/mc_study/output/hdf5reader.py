# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import h5py
import json
import numpy as np

filename = "/Users/ff/Documents/WORK/PROJECTS/2018_DTOceanPlus/Verification/getting_started_example_files-2/RM3 Model Data/wec_solution.h5"

json_ds = {}
with h5py.File(filename, "r") as f:
    # List all groups
    print("Keys: %s" % f.keys())
    for k in f.keys():
      json_ds[k] = f[k][()]

tp = json_ds['te']
hs = json_ds['hm0']
dirs = json_ds['wave_dir']
J = np.zeros((len(tp), len(hs), len(dirs)))
for n in range(len(tp)):
  for m in range(len(hs)):
    for l in range(len(dirs)):
      J[n,m,l] = 500*tp[n]*hs[m]**2

keys = list(json_ds.keys())
for k in keys:
  if isinstance(json_ds[k], (np.ndarray)):
    if np.any(np.iscomplex(json_ds[k])):
      json_ds[k+'_real'] = json_ds[k].real.tolist()
      json_ds[k+'_imag'] = json_ds[k].imag.tolist()
      json_ds.pop(k, None)
    else:
      if k=="user_power_matrix":
        json_ds[k] /= (J*6.0)
      json_ds[k] = json_ds[k].tolist()


print(json.dumps(json_ds))
conversion_inputs = {"capture_width_ratio": "user_power_matrix",
"hs_capture_width":"hm0",
"tp_capture_width":"te",
"wave_angle_capture_width":"wave_dir"}

json_dtop = {}
for k, v in conversion_inputs.items():

    json_dtop[k] = json_ds.get(v)

with open('testJSON_dto_dtop_conversion_inputs.json','w') as outfile:
  json.dump(json_dtop, outfile)

conversion_rules = {
    "wave_frequency": 'periods',
    "wave_direction": 'directions',
    "velocity_transformation_matrix": None,
    "fitting_damping": 'c_fit',
    "fitting_stiffness": 'k_fit',
    "power_matrix": 'user_power_matrix',
    "mass_matrix": 'm_m',
    "added_mass": 'm_add',
    "radiation_damping": 'c_rad',
    "excitation_force_real": 'f_ex_real',
    "excitation_force_imag": 'f_ex_imag',
    "hydrosatic_stiffness": 'k_hst',
    "diffraction_transfer_matrix_imag": 'diffraction_tr_mat_real',
    "diffraction_transfer_matrix_real": 'diffraction_tr_mat_imag',
    "force_transfer_matrix_imag": 'force_tr_mat_real',
    "force_transfer_matrix_real": 'force_tr_mat_imag',
    "amplitude_coefficient_radiation_imag": 'amplitude_coefficient_radiation_real',
    "amplitude_coefficient_radiation_real": 'amplitude_coefficient_radiation_imag',
    "inscribing_cylinder_radius": 'cyl_radius',
    "modes": 'modes',
    "max_truncation_order": 'max_order',
    "truncation_order": 'truncation_order'
}

json_dtop = {}

for k, v in conversion_rules.items():
  json_dtop[k] = json_ds.get(v)

with open('testJSON_dto_dtop_conversion.json','w') as outfile:
  json.dump(json_dtop, outfile)



