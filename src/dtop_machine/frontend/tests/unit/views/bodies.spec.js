// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Bodies from '@/components/BodyDescription/index.vue';

import Vuex from 'vuex'
import axios from 'axios'
import Vue from 'vue'
import { shallowMount, createLocalVue } from "@vue/test-utils";
import ElementUI from 'element-ui';
import { __createMocks as createStoreMocks } from '@/../tests/__mocks__/index.js';
import { Form } from 'element-ui';

jest.mock('@/store')
jest.mock('axios')

describe("Body Description Component", () => {
  let wrapper;
  let getters;
  let store;

  beforeEach( () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    wrapper = shallowMount(Bodies, {
      propsData: {wec: '1'},
      localVue,
      store: createStoreMocks().store,
      stubs: {
        'el-form': Form,
      }
    })
  })

  it("renders", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.vm.single_body).toBe(false);
    expect(wrapper.vm.bodies).toEqual([])
  })

  it("submit form", () => {
    wrapper.vm.submitForm("body");
    expect(wrapper.vm.bodies.length).toEqual(1);
    expect(wrapper.vm.$store.state.wecBodies.length).toEqual(0);
  })
})
