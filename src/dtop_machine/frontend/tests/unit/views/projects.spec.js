// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { shallowMount, createLocalVue } from '@vue/test-utils'

import mcStudies from '@/views/mc_studies/index'
import ElementUI from 'element-ui'

import mcStudyData from '@/../tests/unit/json/RM1_MC3_inputs.json'

import Vuex from 'vuex'
import axios from 'axios'
import Vue from 'vue'

Vue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('NewProject', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let studyId = 1;

  const $router = {
    push: jest.fn(),
  }

  let store = new Vuex.Store({
      state: {
        studyId: null,
        studyTitle: mcStudyData.data.title,
        wecBodies: [],
        wecJoints: [],
        wecBodiesIDs: [],
        completion_state: { general: false, dimensions: false, model: false, results: false },
        status: mcStudyData.data.status,
        complexity: mcStudyData.data.complexity,
        machine_type: mcStudyData.data.type
      }
  })

  const index = 0
  const rows = [{"id": 1,"message": "Project 1"}]

  it('getProjects error', async () => {
      const wrapper = shallowMount(mcStudies, localVue)
      await wrapper.vm.getProjects();
      expect(wrapper.vm.mc_studies).toEqual([]);
  })

  it('getProjects', async () => {
      const wrapper = shallowMount(mcStudies, localVue)
      axios.resolveWith(mcStudyData);
      await wrapper.vm.getProjects();
      expect(wrapper.vm.mc_studies).toEqual(mcStudyData.data);
  })
})
