// This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue';
import Vuex from 'vuex';
import studies from '@/store/modules/studies'

Vue.use(Vuex);

export const getters = {
  studyId: () => 1,
  studyTitle: () => 'test',
  wecBodies: () => [],
  wecBodiesIDs: () => [],
  wecJoints: () => [],
  completion_state: () => {},
  status: () => 0,
  complexity: () => 1,
  machine_type: () => 'wec',
};

export const mutations = {
  SET_ID: jest.fn(),
  SET_TITLE: jest.fn(),
  SET_BODIES: jest.fn(),
  SET_JOINTS: jest.fn(),
  UPDATE_STATUS: jest.fn(),
  SET_TYPE: jest.fn(),
  SET_COMPLEXITY: jest.fn(),
};

export const actions = {
  setId: jest.fn(),
  //set_ProjectStatusAction: jest.fn(),
  setTitle: jest.fn(),
  setBodies: jest.fn(),
  setJoints: jest.fn(),
  updateStatus: jest.fn(),
  setMachine: jest.fn(),
  setComplexity: jest.fn()
};

export const state = {
  studyId: null,
  studyTitle: null,
  wecBodies: [],
  wecJoints: [],
  wecBodiesIDs: [],
  completion_state: { general: false, dimensions: false, model: false, results: false },
  status: 0,
  complexity: 1,
  machine_type: 'tec'
};

export function __createMocks(custom = { getters: {}, mutations: {}, actions: {}, state: {} }) {
  const mockGetters = Object.assign({}, getters, custom.getters);
  const mockMutations = Object.assign({}, mutations, custom.mutations);
  const mockActions = Object.assign({}, actions, custom.actions);
  const mockState = Object.assign({}, state, custom.state);

  return {
    getters: mockGetters,
    mutations: mockMutations,
    actions: mockActions,
    state: mockState,
    store: new Vuex.Store({
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState,
      modules: {
        studies
      }
    })
  };
}

export const store = __createMocks().store;
