# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

# from dtop_machine.business.tidal_converter_complexity1 import TECComplexity1
# from dtop_machine.business.tidal_converter_complexity2 import TECComplexity2
# from dtop_machine.business.tidal_converter_complexity3 import TECComplexity3
# from dtop_machine.business.wave_converter_complexity1 import WECComplexity1
# from dtop_machine.business.wave_converter_complexity2 import WECComplexity2
from dtop_machine.business.wave_converter_complexity3 import WECComplexity3


class MachineFactory():
    
    @staticmethod
    def get_machine(machine_type, machine_complexity):

        try: 
            # if machine_type == "TEC" and machine_complexity == 1:
            #     return TECComplexity1()
            # this options are not used or planned in the near future
            # but they are kept here for a possible future development
            # elif machine_type == "TEC" and machine_complexity == 2:
            #     return TECComplexity2()
            # elif machine_type == "TEC" and machine_complexity == 3:
            #     return TECComplexity3()
            # elif machine_type == "WEC" and machine_complexity == 1:
            #     return WECComplexity1()
            # elif machine_type == "WEC" and machine_complexity == 2:
            #     return WECComplexity2()
            if machine_type == "WEC" and machine_complexity == 3:
                return WECComplexity3()
            raise AssertionError('Machine type and stage not found')
        except AssertionError as _e:
            print(_e)
            raise