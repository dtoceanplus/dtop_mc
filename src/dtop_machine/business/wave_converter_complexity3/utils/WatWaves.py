# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/python2.7
# encoding: utf-8
"""
This module provides several tools which are recurrently used in water-wave problems

.. module:: input
   :platform: Windows
   :synopsis: Input module to DTOcean WP2

.. moduleauthor:: Pau Mercadez Ruiz <pmr@civil.aau.dk>
"""

# Start logging
import logging
module_logger = logging.getLogger(__name__)

from math import tanh, tan, cos, pi, log10
from numpy import array, zeros, ones, meshgrid, where, sqrt, arctan2, real, exp, cosh,\
NaN, float64, int32, ceil, conj
from scipy.special import jv, yv
import subprocess
import os

ro = 1000
g = 9.81

def Dispersion_T(L, const):
    """
    Linear dispersion equation, f=0, for travelling modes
    """

    T,h= const
    A= g*T**2/2/pi
    B= 2*pi*h
    f= L-A*tanh(B/L)
    df= 1-(A*B*(tanh(B/L)**2-1))/L**2
    return f,df

def Newton(func, const, x= 1, feat= (100,1e-6,1e-6)):
    """
    Newton method for solving zeros of non-linear equations
    Args:
        func=f(x)=0 is the non-linear equation
        const is a tuple consisting of the f constants
        x is the first iterate
        feat is a tuple consisting of the max. number of iterates, the tolerance in x and f
    """
    kmax,Tol_x,Tol_f= feat
    found= 0
    k= 0
    while found == 0 and k < kmax:
        k+= 1 # Update k
        " Evaluating f and df "
        f,df= func(x,const)
        " Avoiding dividing by zero "
        if abs(df) < Tol_f*1e-3:
            found= 1
            bl= (0,'Local minimum. log|f| = {:.2f}'.format(log10(abs(f)+1e-99)))
        else:
            " Update x "
            x0= x
            x+= -f/df
            " Check on convergence "
            if abs(x-x0) < Tol_x and abs(f) < Tol_f:
                found= 1
                bl= (1,'Converged in {} iterations and with log|f| = {:.2f}'.format(k,log10(abs(f)+1e-99)))
    " Did not converge "
    if found == 0:
        bl= (0,'Did not converge. log|f| = {:.2f}'.format(log10(abs(f)+1e-99)))
    return x,bl
    
def WNumber(period, depth):
    """
    Calculates wave-number, k0, from wave-period and water depth (T,h).
        Dispersion relationship for travelling modes

    Args:
        period: a float or a list of float numbers. Wave periods
        depth: float(). It is the water depth.
    Outputs:
        wavenumbers
    """
    try :
        L = array([Newton(Dispersion_T, (p, depth))[0] for p in period], dtype=float)
    except TypeError:
        L = Newton(Dispersion_T, (period, depth))[0]
    return 2*pi/L

def len2(x):
    """
    len2 is used to get the len even if the object does not have the method
    """
    try:
        if type(x) == int or type(x) == float or type(x) == int32 or type(x) == float64: return 1
        else : return len(x)
    except TypeError:
        raise TypeError('Attempted len2(x) but type(x) = {} is not supported in this version'.format(type(x)))
