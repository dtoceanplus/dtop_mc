# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/python2.7
# encoding: utf-8
"""
This module contains the main classes used to obtain the solution of the hydrodynamic problem for a given wave energy converter

.. module:: output
   :platform: Windows
   :synopsis: wec_external_module module to DTOcean

.. moduleauthor:: Francesco Ferri <ff@civil.aau.dk>
"""
# External Package
import numpy as np


def angle_wrap(angle,conversion=None):
    """
    anglewrap: wrap the angle in the range 0-360. The input and output format are
    decided based on the conversion type

    Args:
        angle (numpy.ndarray): vector containing the angles to be wrapped in the 0-360 range
        conversion (str): decribes the input and output types:
                            Cases:
                                None: maps from degrees to degrees
                                r2r: maps from radiants to radiants
                                r2d: maps from radiants to degrees
                                d2r: maps from degrees to radiants


    Returns:
        wrap (numpy.ndarray): wrapped angle n agreement with the chosen conversion
    """
    def f (x): return (x%360+360)%360
    if not conversion:
        wrap = f(angle)
    elif conversion=='r2r':
        angle *= 180./np.pi
        wrap = f(angle)
        wrap *= np.pi/180.
    elif conversion=='r2d':
        angle *= 180./np.pi
        wrap = f(angle)
    elif conversion=='d2r':
        wrap = f(angle)
        wrap *= np.pi/180.
    else:
        wrap = angle
        raise ValueError('Anglewrap method: Wrong data conversion type!')

    return wrap


def convert_angle(angle):
    """
    convertangle is used to move forth and back between the angle conventions for the wave case.
    This is done to avoid confusion when defining the different coordinate system.
    Hystorically the metocean condition for waves are given in the following convention
    Wave travelling North-South are tagged 0 deg,
                    West-East are tagged 270 deg.
    This convention does not fit with the selected axis East (x) North (y).

    Args:
        angle [rad]:
            angles to be converted from East-North to South-West or viceversa

    Return:
        same object with rotated coordinate system

    """

    return angle_wrap(-(angle+np.pi/2),'r2r')
