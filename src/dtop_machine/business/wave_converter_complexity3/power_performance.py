# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/python3.7
# encoding: utf-8
"""
This module contains the main class to perform a power matrix fitting for 
for the DTOceanPlus Machine Characterization module

.. module:: output
   :platform: Docker
   :synopsis: wec_external_module module to DTOcean+

.. moduleauthor:: Francesco Ferri <ff@civil.aau.dk>
"""
from dtop_machine.business.wave_converter_complexity3.utils import conversion as cnv
import numpy as np
from scipy.interpolate import LinearNDInterpolator

class PowerMatrixFitting():
    HYDRO_STRUCTURE = ['mass_matrix', 'added_mass', 'radiation_damping', 'hydrostatic_stiffness', 'excitation_force_real', 'excitation_force_imag', 'order', 'wave_frequency', 'wave_direction']
    PERFORMANCE_STRUCTURE = ['pm_fitting', 'mooring_stiffness', 'pto_damping', 'additional_damping', 'additional_stiffness', 'capture_width_ratio', 'hs_capture_width', 'tp_capture_width', 'wave_angle_capture_width', 'wave_spectra', 'heading_angle_span','rated_power']
    def __init__(self, hydro_model, power_performance):
        self.hydro = hydro_model
        self.performance = power_performance

        self.characteristic_dimension = power_performance.pop('characteristic_dimension', 1)
        
        # hydrodynamic parameters
        self.mass_matrix=np.zeros(shape=(1), dtype=float)
        self.wave_frequency=np.zeros(shape=(1), dtype=float)
        self.wave_direction=np.zeros(shape=(1), dtype=float)
        self.added_mass=np.zeros(shape=(1,1,1), dtype=float)
        self.radiation_damping=np.zeros(shape=(1,1,1), dtype=float)
        self.excitation_force=np.zeros(shape=(1,1,1), dtype=complex)
        self.hydrostatic_stiffness=np.zeros(shape=(1,1), dtype=float)
        
        # uder definition of power performance and sea state definitions
        self.tp_capture_width=np.zeros(shape=(1), dtype=float)
        self.hs_capture_width=np.zeros(shape=(1), dtype=float)
        self.wave_angle_capture_width=np.zeros(shape=(1), dtype=float)
        
        # user input from power performance and damping/stiffness
        self.pto_damping=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self.mooring_stiffness=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self.additional_damping=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self.additional_stiffness=np.zeros(shape=(1,1,1,1,1), dtype=float)
        self.capture_width_ratio=np.zeros(shape=(1,1), dtype=complex)
        
        # estimated functions/variables
        self.rao=np.zeros(shape=(1,1), dtype=complex)
        self.power_function=np.zeros(shape=(1,1), dtype=complex)
        self.power_matrix=np.zeros(shape=(1,1), dtype=complex)
        self.cwr_matrix=np.zeros(shape=(1,1), dtype=complex)

        # additional attributes
        self.heading_angle_span=None
        self.pf_interp = None
        self.east_north_directions = None
        self.main_beta = 0.0
        self.spectrum=Spectrum(0, 3.3)
        
        # check if the input dictionaries contains all the required keys
        self.verify_inputs()

        # load the data into the corresponding attrbiutes
        self.loadFromJSONDict({**hydro_model, **power_performance})

    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """
        
        varNames = ["mass_matrix", "wave_frequency", "wave_direction", "added_mass", "radiation_damping",
                    "tp_capture_width", "hs_capture_width", "wave_angle_capture_width", "pto_damping",
                    "mooring_stiffness", "additional_damping", "additional_stiffness", "hydrostatic_stiffness",
                    "capture_width_ratio", "order", "heading_angle_span","rated_power"]
        
        [setattr(self,varName, np.array(data[varName])) for varName in varNames]
        
        self.excitation_force = np.array(data["excitation_force_real"]) + 1j*np.array(data["excitation_force_imag"])

        self.ndof = np.shape(self.mass_matrix)[0]

        self.east_north_directions = np.rad2deg(cnv.convert_angle(self.wave_angle_capture_width))

        self.spectrum.spreading = data['wave_spectra']['angular_spreading_factor']
        self.spectrum.gamma = data['wave_spectra']['peak_enhancement_factor']

        self.main_beta = self.east_north_directions[self.capture_width_ratio.argmax()]

    def verify_inputs(self):
        # convert tp to te
        # if not wdirs_yaw[::2] == wave_angles:
        #     raise ValueError('wave angles and wave direction does not match')
        missing_key = [k for k in self.HYDRO_STRUCTURE if k not in self.hydro.keys()]
        if missing_key:
            raise ValueError(f'The following variables are missing in the hydrodynamic_model dictionary: {missing_key}')

        missing_key = [k for k in self.PERFORMANCE_STRUCTURE if k not in self.performance.keys()]
        if missing_key:
            raise ValueError(f'The following variables are missing in the power_performance dictionary: {missing_key}')

        return True
    
    def get_power_function(self, orientation, relative_wave_angle):
        wave_angles = np.mod(orientation +  relative_wave_angle, 360)
        FR, BETA = np.meshgrid(self.wave_frequency, wave_angles, indexing='ij')
        Z = self.pf_interp(FR, BETA)

        return Z

    def eval_power_matrix(self):
        PM = np.zeros(( len(self.tp_capture_width), ))
        CWR = np.zeros(PM.shape)
    
        orient = np.zeros(self.east_north_directions.shape)
        if self.heading_angle_span == 0:
            orient = np.mod(self.east_north_directions - self.main_beta, 360)
        elif self.heading_angle_span != 360.0:
            orient = (self.east_north_directions - self.main_beta + 180 + 360) % 360 - 180
            semiRange = self.heading_angle_span/2
            orient[np.logical_and(orient>=-semiRange, orient<=semiRange)] = 0
            orient[orient>semiRange] -= semiRange
            orient[orient<-semiRange] += semiRange
            orient = np.mod(orient, 360)
            
        for ii_seastate in range(len(self.tp_capture_width)):
            (S, H, Pwave, Te, dirs) = self.spectrum.jonswap(self.wave_frequency, 
                                        self.hs_capture_width[ii_seastate], 
                                        self.tp_capture_width[ii_seastate])

            pf = self.get_power_function(orient[ii_seastate], dirs)

            PM[ii_seastate] = min(np.sum(pf*H**2), self.rated_power)
            CWR[ii_seastate] = PM[ii_seastate]/(Pwave*self.characteristic_dimension)

        self.cwr_matrix = CWR
        self.power_matrix = PM

    @staticmethod
    def unravel(I, X, Y, Z, x, y, z):
        arr = np.zeros((len(x), len(y), len(z)))
        l = 0
        m = 0
        n = 0
        for ii in range(len(I)):
            l = np.where(x == X[ii])
            m = np.where(y == Y[ii])
            n = np.where(z == Z[ii])
            arr[l,m,n] = I[ii]

        return arr

    def eval_rao(self):
        damping = self.pto_damping[0,0,0,:,:]
        damping += self.additional_damping[0,0,0,:,:]
        
        stiffness = self.additional_stiffness[0,0,0,:,:]
        stiffness += self.mooring_stiffness[0,0,0,:,:]

        w = self.wave_frequency
        zi = -((self.mass_matrix+self.added_mass).T*w**2).T + 1j*0
        zi += ((damping+self.radiation_damping).T*1j*w).T
        zi += self.hydrostatic_stiffness+stiffness

        rao = np.zeros(self.excitation_force.shape) + 1j*0
        for ii in range(np.shape(self.excitation_force)[1]):
            rao[:,ii,:] = np.linalg.solve(zi, self.excitation_force[:,ii,:])
        
        self.rao = rao

    def eval_power_function(self):
        w = self.wave_frequency
        self.power_function = np.dot((0.5*(np.abs(self.rao).T*w)**2).T, self.pto_damping[0,0,0,:,:]).sum(-1)

        # create interpolant for power function
        fr, beta = np.meshgrid(self.wave_frequency, 
                                self.wave_direction.tolist()+[360], indexing='ij')
        src = np.vstack((fr.ravel(), beta.ravel())).T
        self.pf_interp = LinearNDInterpolator(src, np.c_[self.power_function, self.power_function[:,0]].ravel())

    def performance_fitting(self):
        if self.performance['pm_fitting']==True:
            pass
        
        self.eval_rao()
        self.eval_power_function()
        self.eval_power_matrix()

        c_fit_sm = np.zeros(self.pto_damping.shape)
        k_fit_sm = np.zeros(self.pto_damping.shape)
        wec_power_matrix = self.cwr_matrix
    
        return c_fit_sm, k_fit_sm, wec_power_matrix


class Spectrum():
    def __init__(self, spreading, gamma):
        self.spreading = spreading
        self.gamma = gamma
        
    def jonswap(self, wave_frequency, Hs, Tp):
        # function [ S,H,Pwave,Te ] = JONSWAP(w,Tp,Hs,gamma )
        gamma = self.gamma
        nw=len(wave_frequency)
        f=wave_frequency/(2*np.pi)
        A=0.3125*Hs**2/Tp**4
        B=1.25/Tp**4
        fp=1/Tp
        m0=0.
        m_1=0.
        Pwave=0.
        H=np.zeros((nw))
        S=np.zeros((nw))
        for i in range(1,nw):
            fc=0.5*(f[i]+f[i-1])
            df=f[i]-f[i-1]
            S[i]=A/fc**5*np.exp(-B/fc**4)
            if (fc<fp):
                sigma=0.07
            else:
                sigma=0.09
            pa=np.exp(-(fc-fp)**2/(2*sigma**2*fp**2))
            S[i]=S[i]*gamma**pa
            m0=m0+S[i]*df
            m_1=m_1+S[i]/fc*df

        alpha=Hs**2/(16.0*m0)
        Te=m_1/m0
        for i in range(1,nw):
            df=f[i]-f[i-1]
            S[i]=S[i]*alpha
            H[i]=np.sqrt(2.0*S[i]*df)
            Pwave=Pwave+0.25*1025.0*9.81*9.81*H[i]*H[i]/wave_frequency[i]

        if self.spreading > 0:
            H = H.reshape(len(H),1)*np.array([[0.1,0.2,0.5,0.2,0.1]])
            return S, H, Pwave, Te, np.linspace(-self.spreading, self.spreading, 5)

        H = H.reshape((len(H),1))
        return S, H, Pwave, Te, np.array([0.0])