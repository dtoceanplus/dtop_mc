# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

from dtop_machine.business.wave_converter_complexity3.transfers import transfers
from dtop_machine.business.wave_converter_complexity3.utils import hydrostatic, mesh
import logging
import capytaine as cpt
from capytaine.meshes.meshes import Mesh
from capytaine.meshes.geometry import xOz_Plane
from capytaine.meshes.symmetric import ReflectionSymmetricMesh
import xarray as xr
import numpy as np

from rq import get_current_job

LOG = logging.getLogger()

class iBoundaryElementMethod():
    def __init__(self):
        # empty instance, to be filled once the inputs have been validated
        self.inscribing_cylinder = cpt.FloatingBody()
        self.n_theta = None
        self.n_zeta = None
        self.water_depth = None
        self.cylinder_radius = None
        self.cylinder_center = None
        self.cyl_t = np.array([0])
        self.cyl_z = np.array([0])

        self.bodies_def = []
        self.bodies_join = None
        self.bodies = []
        self.meshes = []
        self.meshes_fn = []
        self.dofs = []
        self.omega_range = []
        self.angle_range = []
        self.problems = []
        self.data = None
        self.solve_array_interaction = False
        self.phi = None
        self.n_dofs = None
        self.k_hst = None

        # give defualt value to the interaction matrixs
        self.phi_r = None
        self.phi_s = None

        self.job = None

        # # perform a check on the inputs, and raise an error if the data is incorrect
        # self.validate_inputs()
    def set_inputs(self, inputs):
        self.bodies_def = inputs['bodies']
        self.setup_redis_job()
        # REPETITION: his is validated in the validate_input function
        # for body in inputs['bodies']:
        #     if not os.path.isfile(body['mesh']):
        #         raise IOError(f"The file name {body['mesh']} is not a valid file")

        self.water_depth = inputs['water_depth']
        self.omega_range = inputs['wave_frequency']
        self.angle_range = [np.deg2rad(el) for el in inputs['wave_direction']]
        self.dofs = inputs['dofs']
        self.n_theta = inputs['cyl_theta']
        self.n_zeta = inputs['cyl_zeta']
        self.solve_array_interaction = inputs['get_array_mat']

        self.validate_inputs()

    def validate_inputs(self):
        # validate the input data before performing any calculation
        try:
            if self.water_depth > 1000:
                raise IOError(
                    'Infinite water depth is not supported for the calculation of the array interaction matrices')

            # create the bodies from the list of mesh, assign the dofs and create a collection of meshes
            for ib, body in enumerate(self.bodies_def):
                # if not os.path.isfile(body['mesh']):
                #     raise IOError(f"The file name {body['mesh']} is not a valid file")
                if not body['mesh_raw'][0].split()[0] == '2':
                    raise IOError(
                        f"The file name {body['mesh']} is not a valid file")
                self.meshes_fn.append(body['mesh'])
                body_m = self.decode_DAT(body['mesh_raw'])
                self.meshes.append(body_m)
                body_obj = cpt.FloatingBody(body_m)
                # body_obj = cpt.FloatingBody.from_file(body['mesh'], file_format=body['mesh_format'])
                body_obj.name = f"body_{ib}"
                # body_obj.center_of_mass = body['cog'] # the center of mass is removed because the velcoity transformation matrix will 
                # apply transformation from center of flotation to cog.
                for dof in self.dofs:
                    if dof.lower() in ['surge', 'sway', 'heave']:
                        body_obj.add_translation_dof(name=dof)
                    elif dof.lower() in ['roll', 'pitch', 'yaw']:
                        body_obj.add_rotation_dof(name=dof)
                    else:
                        raise TypeError(
                            f'Invalid Definition of the dof: {dof}')

                self.bodies.append(body_obj)

            # self.bodies[1].translate((50,70,0))

            bodies = self.bodies[0]
            for body in self.bodies[1:]:
                bodies += body

            self.bodies_join = bodies
            self.n_dofs = len(self.bodies_join.dofs.keys())
            print("Merged body dofs:", list(self.bodies_join.dofs.keys()))

            if self.solve_array_interaction:
                # estimate the cylinder dimensions from the colleciton of meshes
                # escluding the z coordinate
                mesh_cog = self.bodies_join.mesh.center_of_mass_of_nodes[:-1]
                self.cylinder_center = mesh_cog
                radius = np.linalg.norm(
                    mesh_cog-self.bodies_join.mesh.vertices[:, :-1], axis=1).max()
                self.cylinder_radius = radius + \
                    np.sqrt(self.bodies_join.mesh.faces_areas.mean())
                self.create_cylinder()
        except:
            raise

    @staticmethod
    def decode_DAT(mesh_string, name=None):
        """Loads Nemoh (Ecole Centrale de Nantes) mesh files.

        Parameters
        ----------
        filename: str
            array with string representing the mesh of the body

        Returns
        -------
        Mesh or ReflectionSymmetry
            the loaded mesh

        Note
        ----
        MAR files have a 1-indexing
        """
        header = mesh_string[0]
        _, symmetric_mesh = header.split()

        vertices = []
        for line in mesh_string[1:]:
            line = line.split()
            if line[0] == '0':
                break
            vertices.append(list(map(float, line[1:])))

        vertices = np.array(vertices, dtype=np.float)
        faces = []
        for line in mesh_string[len(vertices)+2:]:
            line = line.split()
            if line[0] == '0':
                break
            faces.append(list(map(int, line)))

        faces = np.array(faces, dtype=np.int)

        if int(symmetric_mesh) == 1:
            if name is None:
                half_mesh = Mesh(vertices, faces-1)
                return ReflectionSymmetricMesh(half_mesh, plane=xOz_Plane)
            else:
                half_mesh = Mesh(vertices, faces-1, name=f"half_of_{name}")
                return ReflectionSymmetricMesh(half_mesh, plane=xOz_Plane, name=name)
        else:
            return Mesh(vertices, faces-1, name)

    def create_cylinder(self):
        # create the cylinder for the array interaction
        # this is called during the input validation if the solve_array_interaction
        # attribute is set to True

        theta = np.linspace(0, 2*np.pi, self.n_theta, endpoint=False)
        self.cyl_t = theta  # this is later used in the transfer function
        # TODO: check if the logaritmic discretization is correct
        zeta = -np.geomspace(1, self.water_depth+1, self.n_zeta)+1
        self.cyl_z = zeta  # this is later used in the transfer function

        x = np.cos(theta)*self.cylinder_radius + self.cylinder_center[0]
        y = np.sin(theta)*self.cylinder_radius + self.cylinder_center[1]

        vertices = np.concatenate(
            [np.c_[x, y, np.ones(y.shape)*z] for z in zeta])
        n_faces = (self.n_zeta-1)*self.n_theta

        faces = []
        for jj in range(self.n_zeta-1):
            for ii in range(1, self.n_theta):
                faces.append([(jj*self.n_theta)+ii,
                              (jj*self.n_theta)+ii+self.n_theta,
                              (jj*self.n_theta)+ii+self.n_theta+1,
                              (jj*self.n_theta)+ii+1])
            faces.append([(jj*self.n_theta)+1+ii,
                          (jj*self.n_theta)+1+ii+self.n_theta,
                          (jj*self.n_theta)+1+ii+1,
                          (jj*self.n_theta)+1+ii+1-self.n_theta])

        faces = np.array(faces)-1
        self.inscribing_cylinder.mesh = Mesh(vertices=vertices, faces=faces)

    def preprocessing(self):
        # this function is used to setup the problems to be solved
        self.problems = [
            cpt.RadiationProblem(
                body=self.bodies_join, radiating_dof=dof, omega=omega, sea_bottom=-self.water_depth)
            for dof in self.bodies_join.dofs
            for omega in self.omega_range
        ]

        self.problems += [
            cpt.DiffractionProblem(
                body=self.bodies_join, wave_direction=angle, omega=omega, sea_bottom=-self.water_depth)
            for angle in self.angle_range
            for omega in self.omega_range
        ]

    def update_redis_job(self, val):
        if self.job is None:
            return 0
        
        self.job.meta['tta'] = val
        self.job.save_meta()

    def setup_redis_job(self):
        job = get_current_job()
        self.job = job
        if job is not None:
            print('Current job: %s' % (job.id,))
        
        self.update_redis_job(0)

    def solve(self, multibody_mapping):
        # solve the hydrostatic problem first
        self.k_hst = self.solve_hydrostatic()

        # solve the boundary value problems stored in the problems attribute
        # and assemble the outputs
        solver = cpt.Nemoh()

        print('Solving the boundary value problem')
        try:
            results = []
            nprob = len(self.problems)
            if self.solve_array_interaction:
                nprob *= 2
            for ip, problem in enumerate(sorted(self.problems)):
                results.append(solver.solve(problem))
                self.update_redis_job(5+(ip+1)/nprob*75)

            data = cpt.assemble_dataset(results)
            fex = data['diffraction_force'].data + \
                data['Froude_Krylov_force'].data
            fex_ds = data['diffraction_force']
            fex_ds.name = 'excitation_force'
            fex_ds.data = fex
            self.data = xr.merge([data, fex_ds])

            if self.solve_array_interaction:
                print('Calculate the potential at the cylinder')
                (self.phi_r, self.phi_s) = self.cylinder_potential(results)

            self.apply_implict_constraints(self, multibody_mapping)
        except Exception as e:
            LOG.error(str(e))
            raise RuntimeError(f'Calculation interrupted: Error -> {str(e)}')

        print("Calculate force and diffraction transfer matrixs")
        if self.solve_array_interaction:
            (self.diffraction_tr_mat,
             self.force_tr_mat,
             self.amplitude_coefficient_radiation,
             self.order,
             self.truncation_order) = transfers(self.water_depth,
                                                np.array(
                                                    self.angle_range)*np.pi/180.,
                                                2*np.pi /
                                                np.array(self.omega_range),
                                                (self.cylinder_radius,
                                                 self.cyl_t, self.cyl_z),
                                                self.phi_s,
                                                self.phi_r,
                                                self.f_ex)
        else:
            (self.diffraction_tr_mat,
             self.force_tr_mat,
             self.amplitude_coefficient_radiation,
             self.order,
             self.truncation_order) = np.array([0]), np.array([0]), np.array([0]), np.array([0]), np.array([0])

        del results

    @ staticmethod
    def apply_implict_constraints(self, multibody_mapping):
        m_add = self.data['added_mass'].data
        c_rad = self.data['radiation_damping'].data
        f_ex = self.data['excitation_force'].data
        k_hst = self.k_hst

        self.m_add = multibody_mapping.T.dot(m_add.transpose(
            2, 1, 0)).transpose(2, 0, 1).dot(multibody_mapping)
        self.c_rad = multibody_mapping.T.dot(c_rad.transpose(
            2, 1, 0)).transpose(2, 0, 1).dot(multibody_mapping)
        self.f_ex = multibody_mapping.T.dot(
            f_ex.transpose(1, 2, 0)).transpose(2, 1, 0)
        self.k_hst = multibody_mapping.T.dot(k_hst).dot(multibody_mapping)

        # generate a block mass matrix
        M = np.zeros((0))
        for el in range(len(self.bodies_def)):
            mass = np.zeros((6, 6))
            mass[[0, 1, 2], [0, 1, 2]] = self.bodies_def[el]['mass']
            mass[3:, 3:] = self.bodies_def[el]['MoI']
            if el == 0:
                M = self.block_diag(mass[np.ix_(np.where(self.dofs)[0],
                                                np.where(self.dofs)[0])])
            else:
                M = self.block_diag(M, mass[np.ix_(np.where(self.dofs)[0],
                                                   np.where(self.dofs)[0])])

        self.m_m = multibody_mapping.T.dot(M).dot(multibody_mapping)

        if self.solve_array_interaction:
            self.phi_r = multibody_mapping.T.dot(
                self.phi_r.transpose(0, 2, 1, 3)).transpose(1, 0, 2, 3)

    def solve_hydrostatic(self):
        # define the modes or dof of the body
        # TODO: the point of application need to be defined here not hardcoded
        dofs = {'Surge': np.array([1, 1, 0, 0, 0, 0, 0.0]), 'Sway': np.array([1, 0, 1, 0, 0, 0, 0.0]), 'Heave': np.array([1, 0, 0, 1, 0, 0, 0.0]),
                'Roll': np.array([2, 1, 0, 0, 0, 0, 0.0]), 'Pitch': np.array([2, 0, 1, 0, 0, 0, 0.0]), 'Yaw': np.array([2, 0, 0, 1, 0, 0, 0.0])}
        modes = []
        for n_body in range(len(self.bodies_def)):
            body_dofs = [dofs[dof] for dof in self.dofs]
            modes.append([body_dofs, body_dofs])

        geoms = []
        for mesh_obj in self.meshes:
            geom = mesh.surfFromMesh(mesh_obj.vertices, mesh_obj.faces)
            geom.refine_mesh(option=2, triangles=False)
            geoms.append(geom)
        # TODO: use the mesh stored in the body variable
        # geoms = [mesh.readDAT(geom) if geom[-3:].lower()=='dat' else mesh.readGDF(geom) for geom in self.meshes_fn]
        # for geom in geoms:
        #     geom.refine_mesh(option=2, triangles=False)
        cb = [[False, ]]*1*len(geoms)
        cg = [[False, ]]*1*len(geoms)

        Kb, Kg = hydrostatic.eval(geoms, modes, cb, cg)[::2]
        k_hst = Kb + Kg

        return k_hst

    @ staticmethod
    def get_potential_at_vertex(phi_face, vertexes, faces):
        phi_vertex = np.zeros(len(vertexes), dtype=complex)
        counter = np.zeros(len(vertexes))

        for iface, face in enumerate(faces):
            phi_vertex[face] += phi_face[iface]
            counter[face] += 1

        return phi_vertex/counter

    def cylinder_potential(self, results):
        # estimates the potential at the inscribing cylinder for each problem results.
        # the problem results contains the information of the source strengh, therefore
        # without the results the whole BEM need to be solved.

        solver = cpt.Nemoh()

        n_freq = len(self.omega_range)
        n_directions = len(self.angle_range)
        Scattering = 1j * \
            np.zeros((n_freq, n_directions, self.n_zeta, self.n_theta))
        Radiation = 1j * \
            np.zeros((n_freq, self.n_dofs, self.n_zeta, self.n_theta))
        vertices = self.inscribing_cylinder.mesh.vertices
        faces = self.inscribing_cylinder.mesh.faces

        nprob = len(results)*2
        ip = 0
        # the results must be deleted afterward to free memory
        for result in results:
            phi = solver.get_potential_on_mesh(result,
                                               self.inscribing_cylinder.mesh,
                                               chunk_size=self.inscribing_cylinder.mesh.nb_faces+1)

            phi *= 1j*result.omega/result.g
            ifreq = self.omega_range.index(result.problem.omega)

            # TODO: check if the reshape is correct or not
            if type(result) == cpt.bem.problems_and_results.RadiationResult:
                idof = list(self.bodies_join.dofs.keys()).index(
                    result.problem.radiating_dof)
                Radiation[ifreq, idof, :, :] = self.get_potential_at_vertex(
                    phi, vertices, faces).reshape([self.n_zeta, self.n_theta])
                # *(-1j*result.omega) Radiation will be due to unit amplitude motion
                Radiation[ifreq, idof, :, :] *= result.g / \
                    1j/result.omega*(-1j*result.omega)
            else:
                idir = self.angle_range.index(result.problem.wave_direction)
                Scattering[ifreq, idir, :, :] = self.get_potential_at_vertex(
                    phi, vertices, faces).reshape([self.n_zeta, self.n_theta])
                Scattering[ifreq, idir, :, :] *= result.g/1j/result.omega

            # TODO: check the convension problem
            # if convention == 'W' :
            #     Scattering = np.conj(Scattering)
            #     Radiation = np.conj(Radiation)
            self.update_redis_job(5+((ip+1)/nprob+0.5)*75)
            ip += 1

        Scattering[np.isnan(Scattering)] = 0.
        Radiation[np.isnan(Radiation)] = 0.

        return Radiation, Scattering

    def plot_cylinder_potential(self, problem_index, ax=None):
        # Define a mesh of the free surface and compute the free surface elevation
        solver = cpt.Nemoh()
        problem = self.problems[problem_index]
        result = solver.solve(problem)

        potential_at_body = solver.get_potential_on_mesh(
            result, self.inscribing_cylinder.mesh, chunk_size=self.inscribing_cylinder.mesh.nb_faces+1)
        potential_at_body *= 1j*result.omega/result.g

        import matplotlib.pyplot as plt
        import matplotlib.tri as tri
        from mpl_toolkits.mplot3d import Axes3D

        x = self.inscribing_cylinder.mesh.vertices[:, 0]
        y = self.inscribing_cylinder.mesh.vertices[:, 1]
        z = self.inscribing_cylinder.mesh.vertices[:, 2]

        # converts quad elements into tri elements
        def quads_to_tris(quads):
            tris = [[None for j in range(3)] for i in range(2*len(quads))]
            for i in range(len(quads)):
                j = 2*i
                n0 = quads[i][0]
                n1 = quads[i][1]
                n2 = quads[i][2]
                n3 = quads[i][3]
                tris[j][0] = n0
                tris[j][1] = n1
                tris[j][2] = n2
                tris[j + 1][0] = n2
                tris[j + 1][1] = n3
                tris[j + 1][2] = n0
            return tris

        elements_quads = self.inscribing_cylinder.mesh.faces

        # convert all elements into triangles
        elements_all_tris = quads_to_tris(elements_quads)

        # create an unstructured triangular grid instance
        triangulation = tri.Triangulation(x, y, elements_all_tris)
        triangulation.triangles = elements_quads

        flag = True
        # TODO: cannot be triggered by pytest
        # if ax is None:
        #     flag = False
        #     fig, ax = plt.subplots(subplot_kw =dict(projection="3d"))

        self.bodies_join.mesh.show_matplotlib(ax=ax, normal_vectors=True)
        collec = ax.plot_trisurf(triangulation, z, color=(
            0, 0, 0, 0.4), edgecolor='Black', linewidth=0.01)

        colors = np.real(potential_at_body)

        # collec = ax.plot_trisurf(triang, z, cmap=cmap, shade=False, linewidth=0.)
        collec.set_array(colors)
        collec.autoscale()

        # TODO: cannot be triggered by pytest
        # if not flag:
        #     plt.show()

    @ staticmethod
    def block_diag(*arrs):
        """Create a block diagonal matrix from the provided arrays.

        Given the inputs `A`, `B` and `C`, the output will have these
        arrays arranged on the diagonal::

            [[A, 0, 0],
            [0, B, 0],
            [0, 0, C]]

        If all the input arrays are square, the output is known as a
        block diagonal matrix.

        Parameters
        ----------
        A, B, C, ... : array-like, up to 2D
            Input arrays.  A 1D array or array-like sequence with length n is
            treated as a 2D array with shape (1,n).

        Returns
        -------
        D : ndarray
            Array with `A`, `B`, `C`, ... on the diagonal.  `D` has the
            same dtype as `A`.

        References
        ----------
        .. [1] Wikipedia, "Block matrix",
            http://en.wikipedia.org/wiki/Block_diagonal_matrix

        Examples
        --------
        >>> A = [[1, 0],
        ...      [0, 1]]
        >>> B = [[3, 4, 5],
        ...      [6, 7, 8]]
        >>> C = [[7]]
        >>> print(block_diag(A, B, C))
        [[1 0 0 0 0 0]
        [0 1 0 0 0 0]
        [0 0 3 4 5 0]
        [0 0 6 7 8 0]
        [0 0 0 0 0 7]]
        >>> block_diag(1.0, [2, 3], [[4, 5], [6, 7]])
        array([[ 1.,  0.,  0.,  0.,  0.],
            [ 0.,  2.,  3.,  0.,  0.],
            [ 0.,  0.,  0.,  4.,  5.],
            [ 0.,  0.,  0.,  6.,  7.]])

        """
        if arrs == ():
            arrs = ([],)

        arrs = [np.atleast_2d(a) for a in arrs]

        bad_args = [k for k in range(len(arrs)) if arrs[k].ndim > 2]
        if bad_args:
            raise ValueError("arguments in the following positions have dimension "
                             "greater than 2: %s" % bad_args)

        shapes = np.array([a.shape for a in arrs])
        out = np.zeros(np.sum(shapes, axis=0), dtype=arrs[0].dtype)

        r, c = 0, 0
        for i, (rr, cc) in enumerate(shapes):
            out[r:r + rr, c:c + cc] = arrs[i]
            r += rr
            c += cc
        return out


# if __name__ == "__main__":

#     data_cpt = {'bodies':
#                     [
#                         {
#                             'mesh': '/Users/ff/Documents/WORK/PROJECTS/2018_DTOceanPlus/Coding/capytaine/cylinder/nemoh_out/body/Cylinder.dat',
#                             'mesh_format': 'nemoh', 'mass': 1000, 'MoI': np.eye(3)
#                         },
#                         {
#                             'mesh': './test/business/data/Half_Barge.gdf',
#                             'mesh_format': 'gdf',
#                             'ID':[0], 'extended_dof':0, 'cog':[0,0,-30.], 'axis_angles':[0,0,0.],
#                             'parent_body':[-1], 'dof_with_parent':[], 'number_child':1,
#                             'child_dof_position': [[1,0.,0,0]], 'mass': 1000, 'MoI': np.eye(3)
#                         }
#                         # {
#                         #     'mesh': '/Users/ff/Documents/WORK/PROJECTS/2018_DTOceanPlus/Coding/capytaine/cylinder/nemoh_out/body/Cylinder.dat',
#                         #     'mesh_format': 'nemoh'
#                         # }
#                         # {
#                         #     'mesh': './test/business/data/Moonpool.GDF',
#                         #     'mesh_format': 'gdf'
#                         # }
#                     ],
#                 'wave_frequency': [0.1, 0.3, 0.35],
#                 'wave_direction': [0, 180],
#                 'water_depth': 50,
#                 'dofs': ['Heave','Surge'],
#                 'cyl_theta': 10,
#                 'cyl_zeta': 12,
#                 'get_array_mat': False}

#     solver = iBoundaryElementMethod()
#     solver.set_inputs(data_cpt)
#     # solver.validate_inputs()
#     solver.preprocessing()
#     solver.solve(np.eye(2))
#     #solver.plot_cylinder_potential()

#     print(solver.data)

#     solver.bodies += solver.inscribing_cylinder
#     solver.bodies.mesh.show()

#     solver.data.added_mass.shape
