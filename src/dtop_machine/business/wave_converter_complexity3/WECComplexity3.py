# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

from dtop_machine.business.iconverter.IConverter import IConverter
from dtop_machine.business.wave_converter_complexity3.iBEM import iBoundaryElementMethod
from dtop_machine.business.wave_converter_complexity3.multibody_analysis import MultiBodyAnalysis
from dtop_machine.business.wave_converter_complexity3.power_performance import PowerMatrixFitting


from numpy import array, int32
import logging
import numpy as np
import json
from rq import get_current_job


LOG = logging.getLogger(__name__)

class WECComplexity3(IConverter):
    """Instantiation of the IConverter class corresponding to the TEC complexity 1 type.

    Attributes:
        type (str): Type of the array. In this case "TEC".
        complexity (str): Stage of the array. In this case "Stage 1".
    """
    standard_dofs = ['Surge', 'Sway', 'Heave', 'Roll', 'Pitch', 'Yaw']

    def __init__(self):
        LOG.info('... inizialize the WEC3 Sub-Module.')
        super().__init__()
        self.type = "WEC"
        self.complexity = 3
        self.solver = iBoundaryElementMethod()
        self.velocity_transformation_matrix = None
        self.job = None

    def update_redis_job(self, val):
        if self.job is None:
            return 0
        
        self.job.meta['tta'] = val
        self.job.save_meta()

    def setup_redis_job(self):
        job = get_current_job()
        self.job = job
        if job is not None:
            print('Current job: %s' % (job.id,))
        
        self.update_redis_job(0)

    def power_performance_fit(self, hydro_model, 
                                    power_performance): 
        fit_model = PowerMatrixFitting(hydro_model, 
                                       power_performance)  
        c_fit, k_fit, power_matrix = fit_model.performance_fitting()   
        
        pf_results = {"fitting_damping": c_fit,
                      "fitting_stiffness": k_fit,
                      "power_matrix": power_matrix}

        return json.dumps( pf_results, cls=NumpyEncoder)                                    

    def solve_bem_problem(self, data):
        self.setup_redis_job()
        self.update_redis_job(0)
        LOG = logging.getLogger(__name__)
        LOG.info('Create the transformation matrix for the multibody')
        multibody_mapping = self.multibody_dofs_mapping(data)
        self.velocity_transformation_matrix = multibody_mapping
        self.update_redis_job(2)
        self.solver.set_inputs(data)
        self.update_redis_job(3)
        self.solver.preprocessing()
        self.update_redis_job(5)
        LOG.info('Solving the BEM problem')
        self.solver.solve(multibody_mapping)
        self.update_redis_job(80)

        bem_results = self.convert_results()
        self.update_redis_job(90)
        if not data['get_array_mat']:
            LOG.info('Finish')
            return json.dumps(bem_results)
        
        LOG.info('Estimate the Power Matrix of the machine')
        pm_fitting = json.loads(self.power_performance_fit(bem_results, data))

        results = {**bem_results, **pm_fitting}
        LOG.info('Finish')
        self.update_redis_job(100)
        
        return json.dumps(results)  # , cls=NumpyEncoder)  the NumpyEncored is not need the conversion is done at convert_results 

    @staticmethod
    def multibody_dofs_mapping(data):
        bodies = data['bodies']
        mb_obj = MultiBodyAnalysis(data['bodies'], data['shared_dof'], data['point_application'])
        mb_obj.set_multibody_data()
        mb_obj.get_multibody_topology()
        mb_obj.set_joints_position_orientation()
        mb_obj.eval_velocity_transformation_matrix()
        bodies_dofs = mb_obj.get_dofs_floating_bodies()
        return mb_obj.b_mat


    def convert_results(self):
        modes = []
        for el in self.solver.dofs:
            mode = [1, 1, 0, 0, 0, 0, 0]
            if el.lower() == 'surge':
                mode = [1, 1, 0, 0, 0, 0, 0]
            elif el.lower() == 'sway':
                mode = [1, 0, 1, 0, 0, 0, 0]
            elif el.lower() == 'heave':
                mode = [1, 0, 0, 1, 0, 0, 0]
            elif el.lower() == 'roll':
                mode = [2, 1, 0, 0, 0, 0, 0]
            elif el.lower() == 'pith':
                mode = [2, 0, 1, 0, 0, 0, 0]
            elif el.lower() == 'yaw':
                mode = [2, 0, 0, 1, 0, 0, 0]

            modes.append(mode)
        
        return {
                "wave_frequency": self.solver.omega_range,
                "wave_direction": np.rad2deg(self.solver.angle_range).tolist(),
                "velocity_transformation_matrix": self.velocity_transformation_matrix.tolist(),
                "fitting_stiffness": [[[[[0]]]]],
                "fitting_damping": [[[[[0]]]]],
                "power_matrix": [[[0]]],
                'mass_matrix': self.solver.m_m.tolist(),
                'added_mass': self.solver.m_add.tolist(),
                'radiation_damping': self.solver.c_rad.tolist(),
                'hydrostatic_stiffness': self.solver.k_hst.tolist(), 
                'excitation_force_imag': self.solver.f_ex.imag.tolist(), 
                'excitation_force_real': self.solver.f_ex.real.tolist(), 
                'diffraction_transfer_matrix_imag': self.solver.diffraction_tr_mat.imag.tolist(), 
                'diffraction_transfer_matrix_real': self.solver.diffraction_tr_mat.real.tolist(), 
                'force_transfer_matrix_imag': self.solver.force_tr_mat.imag.tolist(),
                'force_transfer_matrix_real': self.solver.force_tr_mat.real.tolist(), 
                'amplitude_coefficient_radiation_imag': self.solver.amplitude_coefficient_radiation.imag.tolist(), 
                'amplitude_coefficient_radiation_real': self.solver.amplitude_coefficient_radiation.real.tolist(), 
                'inscribing_cylinder_radius': self.solver.cylinder_radius, 
                'modes': modes, 
                'order': self.solver.order.tolist(), 
                'max_truncation_order': self.solver.order.tolist(), 
                'truncation_order': self.solver.truncation_order.tolist()
        }
    
    @staticmethod
    def build_multibody_tree(bodies, joints):
        def find_body(id, bodies):
            b = [item for item in bodies if item.get('ID')[0]==id]
            if not b:
                raise RuntimeError('Cannot find a body for the given ID')
            if len(b)>1:
                raise RuntimeError('Multiple bodies found with the same ID')
            return b[0]
        multi_body = bodies
        LOG.info('Multi Body Original')
        LOG.info('*'*100)

        try:
            for joint in joints:
                LOG.info('iterate over the defined joints')
                bp = find_body(joint['parent'], multi_body)
                bc = find_body(joint['child'], multi_body)
                LOG.info('bodies original')
                # LOG.info(bp)
                # LOG.info(bc)
                # work on the parent side
                bp['number_child']+=1
                LOG.info(f'point_of_application: {joint["point_of_application"]}')
                bp['child_dof_position'].append(bc['ID']+joint['point_of_application'])
                LOG.info(f'child_dof_position: {bp["child_dof_position"]}')
                # work on the child side
                bc['extended_dof']+=1
                bc['parent_body'] = [joint['parent']]
                LOG.info(f'parent_body: {bp["parent_body"]}')
                dof_type = 1
                if joint['type']=='hinge':
                    dof_type = 2
                bc['dof_with_parent'].append([dof_type]+joint['joint_axis'])
                LOG.info(f'dof_with_parent: {bp["dof_with_parent"]}')
                LOG.info('bodies reconstructed')
                # LOG.info(bp)
                # LOG.info(bc)

        except Exception as e:

            LOG.error(str(e))
            raise RuntimeError(f'Cannot reconstruct the hierarchy from the Bodies and Joint description. Error: {str(e)}')
        
        LOG.info('Multi Body Reconstructed')
        LOG.info('*'*100)

    @staticmethod
    def verify_project_inputs(inputs):
        LOG.info('Verifying the data and building the multibody structure')
        if isinstance(inputs, (int, float, list)):
            return False
        if len(inputs['bodies'])>1:
            # run this
            LOG.info('Multibody structure detected')
            bodies_id = [el['ID'][0] for el in inputs['bodies']]
            LOG.info(bodies_id)
            if inputs['joints']:
                LOG.info(inputs['joints'])
                # validate multibody definition
                for joint in inputs['joints']:
                    if joint['parent'] not in bodies_id:
                        raise ValueError('The Joint parent ID cannot be found in the list of bodies: Impossible to create the multibody structure')
                    if joint['child'] not in bodies_id:
                        raise ValueError('The Joint child ID cannot be found in the list of bodies: Impossible to create the multibody structure')
                    if joint['parent'] == joint['child']:
                        raise ValueError('The Joint parent and child IDs cannot be the same, check the Joint definition')
            else:
                raise ValueError('The multibody Joint definition is missing, cannot build the multibody structure. Operation Interrupted.')
            
            LOG.info('Calling the static method')
            WECComplexity3.build_multibody_tree(inputs['bodies'], inputs['joints'])
        else:
            LOG.info('Single Body Wave Energy Concerter')
        
        if 'get_array_mat' not in inputs.keys():
            inputs['get_array_mat'] = True
            
        return True  

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)  # cannot be triggered in pytest
# if __name__ == "__main__":

#     data = {'bodies': 
#                     [   
#                         {
#                             'mesh': './test/business/data/Cylinder.dat',
#                             'mesh_format': 'dat',
#                             'ID':[0], 'extended_dof': 0, 'cog':[0,0,-30.], 'axis_angles':[0,0,0.], 
#                             'parent_body':[-1], 'dof_with_parent':[], 'number_child': 0,
#                             'child_dof_position': [], 'mass': 1000, 'MoI': np.eye(3)
#                         }
#                         # {
#                         #     'mesh': './test/business/data/Moonpool.GDF',
#                         #     'mesh_format': 'gdf',
#                         #     'ID':[0], 'extended_dof':0, 'cog':[0,0,-30.], 'axis_angles':[0,0,0.], 
#                         #     'parent_body':[-1], 'dof_with_parent':[], 'number_child':1,
#                         #     'child_dof_position': [[1,0.,0,0]]
#                         # }
#                     ],
#                 'wave_frequency': [0.1],
#                 'wave_direction': [0],
#                 'water_depth': 50, 
#                 'dofs': ['Heave'],
#                 'shared_dof': [0, 0, 1, 0, 0, 0],
#                 'point_application': [0.,0.,0.],
#                 'cyl_theta': 10,
#                 'cyl_zeta': 11,
#                 'get_array_mat': True}

#     # data['bodies'].append({ 'mesh': './test/business/data/Moonpool.GDF',
#     #                         'mesh_format': 'gdf',
#     #                         'ID':[1], 'extended_dof':1, 'cog':[0.,0,0.], 'axis_angles':[0,0,0.], 
#     #                         'parent_body':[0], 'dof_with_parent':[[1, 0, 0, 1.]], 'number_child':0,
#     #                         'child_dof_position': []})
#     wec = WECComplexity3()
#     dummy = wec.verify_project_inputs(1)

#     wec.solve_bem_problem(data)

#     wec.solver.bodies.show()

