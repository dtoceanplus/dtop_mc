# This is is the Machine Characterization (MC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*

from abc import ABCMeta, abstractstaticmethod

class IConverter(metaclass=ABCMeta):
    
    """
    An interface class used to represent a Generic Offshore Converter

    ...

    Attributes
    ----------
    

    Abstract Static Methods
    -------
    get_inputs()
    check_inputs()
    read_digrep()
    write_digrep()
    send_output()
    """
    def __init__(self):
        pass 

    def get_inputs(self):
        """get_inputs static method for the Converter Interface"""

    def check_inputs(self):
        """check_inputs static method for the Converter Interface"""
        pass

    def read_digrep(self):
        """read_digrep static method for the Converter Interface"""
        pass

    def write_digrep(self):
        """write_digrep static method for the Converter Interface"""
        pass

    def send_output(self):
        """send_ouput static method for the Converter Interface"""
