from dtop_machine import business
from data.default_data import input_data, input_data_2body, cylinder_results, performance
from dtop_machine.business.wave_converter_complexity3.utils import WatWaves as ww
from dtop_machine.business.wave_converter_complexity3.multibody_analysis import MultiBodyAnalysis
from dtop_machine.business.wave_converter_complexity3.power_performance import PowerMatrixFitting
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import copy

import pytest

def test_wec_complexity3_hydrostatic():
    Machine = business.MachineFactory.get_machine('WEC', 3)

    data_quality = Machine.verify_project_inputs(input_data_2body)
    assert data_quality 
    k_hst = Machine.solver.solve_hydrostatic()

def test_wec_complexity3_init():
    Machine = business.MachineFactory.get_machine('WEC', 3)
    assert Machine.complexity == 3

    with pytest.raises(AssertionError) as excinfo:
        Machine = business.MachineFactory.get_machine('TEC', 0)

def test_wec_complexity3_verify_data():
    Machine = business.MachineFactory.get_machine('WEC', 3)
    data_quality = Machine.verify_project_inputs(1)
    assert data_quality == False

    data_2body = copy.deepcopy(input_data_2body)
    joints = data_2body.pop('joints')
    with pytest.raises(KeyError) as excinfo:
        data_quality = Machine.verify_project_inputs(data_2body)

def test_wec_complexity3_power_matrix_fitting():
    Machine = business.MachineFactory.get_machine('WEC', 3)
    
    Machine.power_performance_fit(cylinder_results, performance)

def test_wec_complexity3_capytaine():
    Machine = business.MachineFactory.get_machine('WEC', 3)

    data_quality = Machine.verify_project_inputs(input_data)
    assert data_quality 

    with pytest.raises(TypeError) as excinfo:
       Machine.solve_bem_problem(1)

    bem_outputs = Machine.solve_bem_problem(input_data)
    assert "added_mass" in bem_outputs

    Machine = business.MachineFactory.get_machine('WEC', 3) 
    bem_outputs = Machine.solve_bem_problem(input_data_2body)
    fig, ax = plt.subplots(subplot_kw =dict(projection="3d"))
    Machine.solver.plot_cylinder_potential(0, ax=ax)

    input_data['get_array_mat'] = False  
    Machine = business.MachineFactory.get_machine('WEC', 3) 
    Machine.solve_bem_problem(input_data)

    with pytest.raises(IOError) as excinfo:
       input_data['water_depth'] = 1001
       Machine.solve_bem_problem(input_data)

    input_data['water_depth'] = 50    
    with pytest.raises(IOError) as excinfo:
       input_data['bodies'][0]['mesh_raw'] = 'error'
       Machine.solve_bem_problem(input_data)

    Machine.solver.block_diag()   
    with pytest.raises(ValueError) as excinfo:
       Machine.solver.block_diag(np.zeros((2,2,2)))

def test_MultiBodyAnalysis():
    body = MultiBodyAnalysis([{'ID': 0}], [1,0,0,0,0,0], np.array([0,0,0]))

    n_bodies = 2
    loose_dof_b = 3
    loose_dof = loose_dof_b*n_bodies

    lCS = [0.,0.,0.]
    shared_dof_bin = [1, 0, 1, 0, 1, 0]
    floating_flag = 1 
    
    # wavebob
    b0 = {'ID':[0], 'extended_dof':0, 'cog':[0,0,-30.], 'axis_angles':[0,0,0.], 
          'parent_body':[-1], 'dof_with_parent':[], 'number_child':1,
          'child_dof_position': [[1,0.,0,0]]}
          
    b1 = {'ID':[1], 'extended_dof':1, 'cog':[0.,0,0.], 'axis_angles':[0,0,0.], 
          'parent_body':[0], 'dof_with_parent':[[1, 0, 0, 1.]], 'number_child':1,
          'child_dof_position': [[2,0.,0,0]]}

    b2 = {'ID':[2], 'extended_dof':1, 'cog':[0.,0,0.], 'axis_angles':[0,0,0.], 
          'parent_body':[1], 'dof_with_parent':[[2, 0, 0, 1.]], 'number_child':0,
          'child_dof_position': []}      
         
    
    bodies = [b0, b1, b2]
    obj = MultiBodyAnalysis(bodies, shared_dof_bin, lCS)
    obj.set_multibody_data()
    obj.get_multibody_topology()
    obj.set_joints_position_orientation()
    obj.eval_velocity_transformation_matrix()
    bodies_dofs = obj.get_dofs_floating_bodies()

    # wavebob
    b0 = {'ID':[0], 'extended_dof':0, 'cog':[0,0,-30.], 'axis_angles':[0,0,0.], 
          'parent_body':[-1], 'dof_with_parent':[], 'number_child':0,
          'child_dof_position': []}    
    obj = MultiBodyAnalysis([b0], shared_dof_bin, lCS)
    obj.set_multibody_data()
    obj.get_multibody_topology()
    obj.set_joints_position_orientation()
    obj.eval_velocity_transformation_matrix()
    bodies_dofs = obj.get_dofs_floating_bodies()

    bodies = [{'ID': [0], 'extended_dof': 0, 'cog': [0, 0, -30.0], 'axis_angles': [0, 0, 0.0], 'parent_body': [-1], 'dof_with_parent': [], 'number_child': 1, 'child_dof_position': [[1, 0.0, 0, 0]]}, {'ID': [1], 'extended_dof': 1, 'cog': [0.0, 0, 0.0], 'axis_angles': [0, 0, 0.0], 'parent_body': [0], 'dof_with_parent': [[1, 0, 0, 1.0]], 'number_child': 0, 'child_dof_position': []}]
    path_matrix = np.array([[ 0. , 1.],[ 0. , -1.]])
    obj.ordering_multibody_tree(bodies, path_matrix, max_iterations=-1)

def test_transfer():
    business.wave_converter_complexity3.transfers.max_trunc_order(np.ones((4,4)), 1, -1)

def test_WatWaves():
    assert type(ww.len2(1)) == int
    assert type(ww.len2([1,1])) == int
    with pytest.raises(TypeError) as excinfo:
        ww.len2(pd.DataFrame)

    (x, bl) = ww.Newton(ww.Dispersion_T, (1, -1))
    assert bl[0] == 0

    (x, bl) = ww.Newton(ww.Dispersion_T, (1, 10), feat = (100, 1e-6, 1e6))
    assert bl[0] == 0

def test_iConverter():
    iCon = business.iconverter.IConverter.IConverter()  
    assert iCon.check_inputs() == None
    assert iCon.read_digrep() == None
    assert iCon.write_digrep() == None