from dtop_machine import business
from data.default_data import input_data, input_data_2body, cylinder_results, performance
from dtop_machine.business.wave_converter_complexity3.utils import hydrostatic
from dtop_machine.business.wave_converter_complexity3.utils import mesh

import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

import pytest

def test_hydrostatic():
    meshes = [el['mesh'] for el in input_data_2body['bodies']]
    dofs = {'Surge': np.array([1,1,0,0,0,0,0.0]), 'Sway': np.array([1,0,1,0,0,0,0.0]), 'Heave': np.array([1,0,0,1,0,0,0.0]),
                'Roll': np.array([2,1,0,0,0,0,0.0]), 'Pitch': np.array([2,0,1,0,0,0,0.0]), 'Yaw': np.array([2,0,0,1,0,0,0.0])}
    modes = []
    for n_body in range(len(meshes)):
        body_dofs = [dofs[dof] for dof in ['Heave', 'Pitch']]
        modes.append([body_dofs, body_dofs])

    # TODO: use the mesh stored in the body variable
    geoms = [mesh.readDAT(geom) if geom[-3:].lower()=='dat' else mesh.readGDF(geom) for geom in meshes]

    cg = [np.array([0.,0,0])]*2
    cb = [np.array([0.,0,0])]*2
    geoms[0].coord[0][0,-1] = 1
    hydrostatic.eval(geoms, modes, cb, cg)
    hydrostatic.eval(geoms, modes, cb, cg, fixedA=True)

def test_mesh():
    meshes = [el['mesh'] for el in input_data_2body['bodies']]
    geoms = [mesh.readDAT(geom) if geom[-3:].lower()=='dat' else mesh.readGDF(geom) for geom in meshes]
    for geom in geoms:
        geom.refine_mesh(option=2, triangles=False)
    
    refpoints = np.array([[10000,0,0],[100.0,0,0]])
    geom.Chkud(refpoints)

    geom.info()
    geom.refine_mesh(option=1, triangles=True)
    geom.NCA()

    pan = geom.coord[0]
    pan[0] = pan[3]
    geom.coord[0] = pan
    pan = geom.coord[1]
    pan[1] = pan[2]
    geom.coord[1] = pan
    geom.triangles()

    refpoints = np.array([[0,0,0],[100.0,0,0]])
    geom.Chkud(refpoints)

    refpoints = np.array([[10000,0,0],[100.0,0,0]])
    geom.Chkud(refpoints)


    


