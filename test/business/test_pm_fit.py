from dtop_machine import business
from dtop_machine.business.wave_converter_complexity3 import utils
from dtop_machine.business.wave_converter_complexity3.multibody_analysis import MultiBodyAnalysis
from dtop_machine.business.wave_converter_complexity3.power_performance import PowerMatrixFitting

import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

import pytest  
from data.default_data import cylinder_results, performance

def test_power_matrix_fitting():

    obj = PowerMatrixFitting(cylinder_results, performance)
    c_fit, k_fit, power_matrix = obj.performance_fitting()

    performance['power_matrix'] = [[[1000.0]]]
    obj = PowerMatrixFitting(cylinder_results, performance)
    c_fit, k_fit, power_matrix = obj.performance_fitting()

    with pytest.raises(ValueError) as excinfo:
        obj = PowerMatrixFitting({'wrong': 'key'}, performance)

    with pytest.raises(ValueError) as excinfo:
        obj = PowerMatrixFitting(cylinder_results, {'wrong': 'key'})  

    with pytest.raises(IndexError) as excinfo:
        cylinder_results['mass_matrix'] = 1
        obj = PowerMatrixFitting(cylinder_results, performance)  
    cylinder_results['mass_matrix'] = [[785000.0]]

def test_conversion():
    # angle wrap
    utils.conversion.angle_wrap(0)
    a = utils.conversion.angle_wrap(np.pi/2, conversion='r2d')
    assert a == 90.0
    a = utils.conversion.angle_wrap(180, conversion='d2r')
    assert a == np.pi
    with pytest.raises(ValueError) as excinfo:
        utils.conversion.angle_wrap(0, conversion='wrong type')

    # # convert tp to te
    # utils.conversion.convert_tp2te(5, 'Jonswap', gamma = 3)
    # utils.conversion.convert_tp2te(5, 'Jonswap', gamma = 10)
    # # convert te to tp
    # utils.conversion.convert_te2tp(5,'Jonswap', gamma=10)

    # # angle definition with yaw
    # with pytest.raises(IOError) as excinfo:
    #     utils.conversion.set_wdirs_with_yaw(np.array([0,1,1,3]), 1, 0, 0)

    # a = utils.conversion.set_wdirs_with_yaw(np.array([1.0,3]), -1, 0, 0)  
    # assert a == [0.0, (1.0, 3.0)]  
    # a = utils.conversion.set_wdirs_with_yaw(np.array([0.0,1.0,3]), -1, 0, 0) 
    # assert len(a) == 2
    # assert len(a[1]) == 3
    # a = utils.conversion.set_wdirs_with_yaw(np.array([0.0,1.0,3]), -1, 0, 180/180*np.pi) 
    # assert a == [0.0, (0.0,), 1.0, (1.0,), 3.0, (3.0,)] 

    # utils.conversion.set_wdirs_with_yaw(np.array([0.0,1,3]), 1, 0, 15.0/180*np.pi) 
    # utils.conversion.set_wdirs_with_yaw(np.array([1.0,3]), -1, 0, 15.0/180*np.pi)  
    # utils.conversion.set_wdirs_with_yaw(np.array([1.0,3]), 10, 0, 15.0/180*np.pi)    
    # utils.conversion.set_wdirs_with_yaw(np.linspace(0, np.pi, 20), 10, 0, 15.0/180*np.pi)  

def test_struc_dyn():
    NBo=1 
    B= np.array([0,180.])/180.*np.pi 
    Hs = np.array([0.5,1,1.5,2.]) 
    Tp = np.array([3.5,4.5,5.5,6.5,7.5,8.5,9.5]) 
    wdir = np.linspace(0,360,30,endpoint=False)/180.*np.pi 
    period = np.linspace(2,15,50) 
    ScatDiag = (np.ones((7,4,2))/(7*4*2),('Jonswap',3.3,0)) 
    M = np.eye(3) 
    Madd = np.array([np.eye(3)]*50) 
    Cpto = np.array([[[np.eye(3)]*2]*4]*7) 
    Crad = np.array([np.eye(3)]*50) 
    Khyd = np.eye(3) 
    Fex = np.ones((50,30,3)) 
    Kfit = np.array([[[np.eye(3)]*2]*4]*7) 
    Cfit = np.array([[[np.eye(3)]*2]*4]*7)

    # p = utils.StrDyn.scatterdiagram_threshold(ScatDiag[0], threshold=1) 

    # Pyr, P_dev = utils.StrDyn.EnergyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
    #                                             M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
    #                                             Kfit, Cfit) 

    # ScatDiag = (np.ones((7,4,2))/(7*4*2),('Jonswap',3.3,2)) 
    # Pyr, P_dev = utils.StrDyn.EnergyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
    #                                             M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
    #                                             Kfit, Cfit)  

    # with pytest.raises(IOError) as excinfo:
    #     ScatDiag = (np.ones((7,4))/(7*4*2),('Jonswap',3.3,0))
    #     Pyr, P_dev = utils.StrDyn.EnergyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
    #                                             M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
    #                                             Kfit, Cfit) 
    # with pytest.raises(IOError) as excinfo:
    #     ScatDiag = (np.ones((7,4,2))/(7*4),('Jonswap',3.3,0))
    #     Pyr, P_dev = utils.StrDyn.EnergyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
    #                                             M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
    #                                             Kfit, Cfit)                                                  

def test_watwaves():
    assert type(utils.WatWaves.len2(1)) == int
    assert type(utils.WatWaves.len2([1,1])) == int
    with pytest.raises(TypeError) as excinfo:
        utils.WatWaves.len2(None)

    (x, bl) = utils.WatWaves.Newton(utils.WatWaves.Dispersion_T, (1, -1))
    assert bl[0] == 0

    (x, bl) = utils.WatWaves.Newton(utils.WatWaves.Dispersion_T, (1, 10), feat = (100, 1e-6, 1e6))
    assert bl[0] == 0

    utils.WatWaves.WNumber(1, 100)

# def test_wave_spectra():
#     Nf = 10 
#     fstr = 0 
#     fstp = 0.5 
#     f = np.linspace(fstr,fstp,Nf) 
#     df = f[1]-f[0] 
#     fp = 1/9. 
#     Hs = 1. 
#     Wave_sp = utils.spec_class.wave_spec(f, fp, Hs, st="Jonswap") 

#     with pytest.raises(IOError) as excinfo:
#         Wave_sp = utils.spec_class.wave_spec(f,fp,Hs,t=1)  

#     Wave_sp = utils.spec_class.wave_spec(f,fp,Hs, t=np.array([0., 180]))
    
#     Wave_sp.spec_type = "Regular"
#     Wave_sp.add_spectrum()
#     Wave_sp.rm_spectrum()
#     Wave_sp.spec_type = "Bretschneider_Mitsuyasu"
#     Wave_sp.add_spectrum()
#     Wave_sp.spec_type = "Modified_Bretschneider_Mitsuyasu"
#     Wave_sp.add_spectrum()
#     Wave_sp.spec_type = "Pierson_Moskowitz"
#     Wave_sp.add_spectrum()
#     Wave_sp.fp = 100
#     Wave_sp.CutOffFactorLowBound = 0
#     Wave_sp.CutOffFactorTopBound = 1e6
#     Wave_sp.CutOffFreqLowBound = 0
#     Wave_sp.CutOffFreqTopBound = 1e6
#     Wave_sp.add_spectrum()
#     Wave_sp.spec_type = "Pierson_Moskowitz"
#     Wave_sp.add_spectrum()
#     Wave_sp.spec_type = "pscSwell"
#     Wave_sp.add_spectrum()
#     Wave_sp.spec_type = "pscSwell"
#     Wave_sp.fp = 0.1
#     Wave_sp.CutOffFactorLowBound = 0
#     Wave_sp.CutOffFactorTopBound = 1e6
#     Wave_sp.CutOffFreqLowBound = 0
#     Wave_sp.CutOffFreqTopBound = 1e6
#     Wave_sp.add_spectrum()
#     Wave_sp.rm_spectrum(index='all')

#     with pytest.raises(ValueError) as excinfo:
#         Wave_sp.spec_type = ""
#         Wave_sp.add_spectrum()

    
#     nt = 13 
#     Wave_sp.t = np.linspace(-np.pi,np.pi,nt) 
#     Wave_sp.spec_type = "Jonswap" 
#     Wave_sp.add_spectrum() 
 
#     s_vec = np.linspace(1,16,5) 
#     for s in s_vec: 
#         Wave_sp.s = s 
#         Wave_sp.add_spectrum() 
