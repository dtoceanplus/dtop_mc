# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest

from dtop_machine.service.db import *
from dtop_machine.service.schemas import *
from dtop_machine.service.models import *


def test_validate_model_schema(app):
    model = {
        "cp": 0.3,
        "number_rotor": 1
    }

    validate_numerical_model_schema(model, 1, 'TEC')
    with pytest.raises(ValueError) as excinfo:
        model['wrong_key'] = 'Test'
        validate_numerical_model_schema(model, 1, 'TEC')

        model.pop('wrong_key')
        model.pop('cp')
        validate_numerical_model_schema(model, 1, 'TEC')


def test_update_results(app):
    with app.app_context():
        results = Results(project_id=1, results={})
        results.update(results={})
        results = Results(project_id=1, results=None)
        data = results.results


def test_init_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False
    def fake_init_db():
        Recorder.called = True
    monkeypatch.setattr('dtop_machine.service.db.init_db', fake_init_db)
    result = runner.invoke(args=['init-db'])
    assert 'Initialized' in result.output
    assert Recorder.called

    

