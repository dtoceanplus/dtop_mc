import os

import pytest

from dtop_machine.service import create_app
from dtop_machine.service.db import init_db


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': os.environ['DATABASE_URL'],
        'SERVER_NAME': 'localhost.dev'
    })
    with app.app_context():
        init_db()
    yield app

@pytest.fixture
def client(app):
    return app.test_client()
    
@pytest.fixture
def runner(app):
    return app.test_cli_runner()
