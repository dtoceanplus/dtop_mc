import sys
import os

import requests
import json
import binascii



sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")


body = {
    "title": "Test array",
    "desc":
    "Simple project to test potential of Wave Energy Converter in the North Sea",
    "complexity": 1,
    "type": "WEC",
    "date": "27-12-2019",
    "status": 20,
    "tags": "wave"
}

body_farm = {
    "number-devices": 2,
    "q_factor": 0.98,
    "aep": 100001,
    "captured_power": 100002,
    "captured_power_per_condition": {
        "siteConditionID": [0],
        "capturedPower": [10001]
    },
    "resource_reduction": 0.97
}

body_device = {
    "northing": 0,
    "easting": 0,
    "q_factor": 0,
    "aep": 0,
    "captured_power": 0,
    "captured_power_per_condition": {
        "siteConditionID": [0],
        "capturedPower": [0]
    }
}


def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper


def remove_all_projects(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    projects_ids = get_project_ids(tr)
    for ecId in projects_ids:
        requests.delete(f'{protocol}//{host}:{port}/mc/{ecId}')


def get_project_ids(tr):
    existing_project = check_project(tr)
    ids = [pr['id'] for pr in existing_project]
    return list(set(ids))


def get_devices_id(tr, ecId):
    existing_project = check_project(tr)
    prj = [pr for pr in existing_project if pr['id'] == ecId]
    if prj:
        print(prj)
        return prj[0]['devices']

    return []


def check_project(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    existing_project = requests.get(f'{protocol}//{host}:{port}/mc').json()
    return existing_project


def post_a_project(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/mc', json=body)
    print('POST A PROJECT')
    print(body)
    print(f'{protocol}//{host}:{port}/mc')
    print(r.status_code)
    print(r)


def post_a_farm(tr, ecId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/mc/{ecId}/farm',
                      json=body_farm)
    print('POST A FARM')
    print(body_farm)
    print(f'{protocol}//{host}:{port}/mc/{ecId}/farm')
    print(r.status_code)
    print(r)


def post_a_device_list(tr, ecId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/mc/{ecId}/devices',
                      json=[body_device, body_device])
    print('POST A LIST OF DEVICES')
    print([body_device, body_device])
    print(f'{protocol}//{host}:{port}/mc/{ecId}/devices')
    print(r.status_code)
    print(r)


def get_a_project(tr):
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        project = existing_project[0]
    else:
        post_a_project(tr)
        projects = check_project(tr)
        if projects:
            project = projects[0]
        else:
            print('ERROR NO PROJECT AVAILABLE IN THE DB')

    return project


def get_a_project_wo_farm(tr):
    print('get_a_project_wo_farm')
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        for project in existing_project:
            if not project['farm']:
                print('Found a project without a farm')
                return project

    print('No project without a farm')
    print('posting a new project')
    post_a_project(tr)
    project = get_a_project_wo_farm(tr)
    print(project)

    return project


def get_project_w_device(tr):
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        for project in existing_project:
            if project['device']:
                print('Found a project with a device')
                return project
        else:
            post_device()

    post_a_project(tr)

def prepare_multipart_formdata(fields, filename):
    fn = os.path.join('test', 'dredd', filename)
    if os.path.isfile(fn):
        print (fn + " exists")
    else:
        print (fn + " doesn't exist")
        exit()

    with open(fn, 'r') as f:
        content_dict = json.load(f)

    boundary = binascii.hexlify(os.urandom(16)).decode('ascii')

    body = (
        "".join("--%s\r\n"
                "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n"
                "Content-Type: application/json\r\n"
                "\r\n"
                "%s\r\n" % (boundary, field, filename, json.dumps(content_dict))
                for field, value in fields.items()) +
        "\r\n--%s--\r\n" % boundary
    )

    content_type = "multipart/form-data; boundary=%s" % boundary

    return content_type, body
