FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY manage.py .
COPY setup.py .
COPY src/instance/hydro_db.json ./default_data/
COPY src/instance/default_projects.json ./default_data/
COPY src/instance/init.db ./src/instance/

COPY src/dtop_machine/ ./src/dtop_machine/

RUN apt update && \
    apt install --yes --no-install-recommends fort77 gfortran libgfortran5 libgbm-dev libgomp1 && \
    pip install numpy && pip install --requirement requirements.txt && \
    pip install --editable . && \
    pip install numpy --upgrade && \
    pip install --upgrade scipy && \
    pip install dredd-hooks && \
    pip install pytest pytest-cov python-dotenv && \
    python -c "import dtop_machine as lib; print(lib)" && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes fort77 gfortran

ENV FLASK_APP dtop_machine.service

EXPOSE 5000


# FROM continuumio/miniconda3:latest
# WORKDIR /app

# COPY dependency.yml .
# RUN conda env create -f dependency.yml

# RUN echo "source activate dtop_machine_env" >> ~/.bashrc
# ENV PATH /opt/conda/envs/dtop_machine_env/bin:$PATH

# COPY manage.py .
# RUN pip --no-cache-dir install pytest pytest-cov

# COPY dtop-shared-library ./dtop-shared-library
# RUN cd dtop-shared-library && pip install .
# RUN python -c "import dtop_shared_library as lib; print(lib)"

# COPY src/dtop_machine/service ./src/dtop_machine/service
# COPY src/dtop_machine/business ./src/dtop_machine/business
# COPY setup.py .
# RUN pip install -e .
# RUN python -c "import dtop_machine as lib; print(lib)"

# COPY .flaskenv .
# ENV FLASK_APP dtop_machine.service

# EXPOSE 5000

# ENV DATABASE_URL sqlite://///db/machine.db