FROM continuumio/miniconda3:latest
WORKDIR /app

COPY dependency.yml .
RUN conda env create -f dependency.yml

RUN echo "source activate dtop_machine_env" >> ~/.bashrc
ENV PATH /opt/conda/envs/dtop_machine_env/bin:$PATH

RUN pip --no-cache-dir install pytest pytest-cov

COPY dtop-shared-library ./dtop-shared-library
RUN cd dtop-shared-library && pip install .
RUN python -c "import dtop_shared_library as lib; print(lib)"

COPY src ./src
COPY setup.py .
RUN pip install -e .
RUN python -c "import dtop_machine as lib; print(lib)"

COPY .flaskenv .
ENV DATABASE_URL sqlite://///db/machine.db

