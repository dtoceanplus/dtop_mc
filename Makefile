# TO SOLVE THE PROBLME WITH PRINTF IN OSX INSTALL FINDUTILS AND CORRECT THE PATH AS:
# PATH=$(brew --prefix)/opt/findutils/libexec/gnubin:$PATH

SHELL = bash
.PHONY: $(wildcard x-*)
export MODULE_NICKNAME = mc

# all: x-env x-dredd
verify:
	docker-compose --project-name mc -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name mc -f verifiable.docker-compose -f verifier.docker-compose stop mc
	docker cp `docker-compose --project-name mc -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
	docker-compose --project-name mc -f verifiable.docker-compose -f verifier.docker-compose down

dist:
	mkdir -p dist
	rm -rf dist/*
	# # This command can only be used in cmd shell!!!
	# docker run --rm -v "%cd%":/code --workdir /code python:3 python setup.py sdist clean --all
	# This command can be used in windows powershell. (Preferred!)
	docker run --rm -v ${PWD}:/code --workdir /code python:3 python setup.py sdist clean --all

dist-shared:
	mkdir -p dist-shared
	rm -rf dist-shared/*
	# # This command can only be used in cmd shell!!!
	# docker run --rm -v "%cd%":/code --workdir /code python:3 python setup.py sdist clean --all
	# This command can be used in windows powershell. (Preferred!)
	docker run --rm -v ${PWD}:/code --workdir /code python:3 python setup.py sdist clean --all

all: dist pytest dredd e2e-cypress


build:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build

build-%:
	echo "build container for" $*
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build $*

run-be:
	docker-compose -f ci/backend.docker-compose -f ci/client.docker-compose -f ci/e2e-cypress.docker-compose up backend

run:
	docker-compose -f ci/backend.docker-compose -f ci/client.docker-compose -f ci/e2e-cypress.docker-compose up backend nginx client mc_worker mc_redis
	#docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` up client backend nginx mc_worker mc_redis
	
stop:
	docker-compose -f ci/backend.docker-compose -f ci/client.docker-compose -f ci/e2e-cypress.docker-compose down --remove-orphans
pytest: build-pytest build-worker pytest-nodeps
pytest-nodeps:
	docker-compose --file ci/pytest.docker-compose --file ci/backend.docker-compose \
	up --abort-on-container-exit --exit-code-from=pytest pytest mc_worker mc_redis

dev: build-backend
	docker-compose --file ci/backend.docker-compose run --rm backend bash

dredd: build-backend build-worker build-dredd dredd-nodeps
dredd-nodeps:
	touch dredd-hooks-output.txt
	docker-compose --file ci/backend.docker-compose --file ci/dredd.docker-compose \
	up --remove-orphans --force-recreate --always-recreate-deps --abort-on-container-exit --exit-code-from=dredd \
	backend dredd mc_worker mc_redis

e2e-cypress: build-backend build-client build-e2e-cypress e2e-cypress-nodeps
e2e-cypress-nodeps:
	docker-compose --file ci/backend.docker-compose --file ci/e2e-cypress.docker-compose \
	--file ci/client.docker-compose \
	up --remove-orphans \
	--force-recreate --always-recreate-deps --abort-on-container-exit \
	--exit-code-from=e2e-cypress nginx e2e-cypress backend mc_worker mc_redis

#####
#####
#####
## old
#####
#####
#####

test: x-dev-install

check: x-dev-install x-check-cov x-check-pylint x-check-commit

x-api:
	$(MAKE) --directory=api xtest
	cp -f ../api-coop/dtop-mc/0.0.1/openapi.json ./src/dtop_machine/service/static

x-env:
	pip install pipenv
	npm install dredd sync-request

x-dredd:
	node_modules/dredd/bin/dredd --config dredd-local.yml

x-be-build-dredd:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml build mc dredd

x-be-dredd:
	touch hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --exit-code-from=dredd mc dredd worker results-getter

x-be-img:
	docker build --tag mc --file ./mc.dockerfile .

x-be-dev: x-be-img
	docker run --tty --interactive --name=dtop-mc-container mc bash

x-be-run: x-be-img
	docker run --detach -p 5000:5000 mc

x-be-test: x-be-img
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --exit-code-from=test-mc mc test-mc

x-pytest:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --exit-code-from=pytest worker pytest

test-create-task:
	apk add bash
	wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -P /tmp
	chmod +x /tmp/wait-for-it.sh
	bash /tmp/wait-for-it.sh mc:5000
	echo '{"id": 10, "title": "PostMan test1", "desc": "Testing PostMan request software", "type": "WEC", "date": "23-12-2019", "status": 20, "tags": "wave", "complexity": 3, "model": 1}' | http --print=HBhb --check-status mc:5000/mc
	echo '{"capture_width_ratio": [[[0.3]]], "tp_capture_width": [5], "hs_capture_width": [2], "wave_angle_capture_width": [0], "pto_damping": [[[1e6]]], "mooring_stiffness": [[[2e6]]], "additional_damping": [[[1e6]]], "additional_stiffness": [[[2e6]]], "wave_spectra": { "spectrum_type": "JONSWAP", "peak_enhancement_factor": 3.3, "angular_spreading_factor": 3 }, "heading_angle_span": 30, "water_depth": 50, "shared_dof": [0,0,1,0,0,0], "local_cs": [0,0,0], "frequency_defition": [0.1,0.5,1], "wave_frequency": [0.1], "wave_direction": [0], "dofs": ["Heave"], "point_application": [0,0,0.0], "angle_definition": [0,180], "cyl_theta": 11, "cyl_zeta": 10, "get_array_mat": true, "ndof": 1, "pto_dof": 1, "mooring_dof": 1, "bodies": [ {	 "ID": [0], "axis_angles": [0,0,0], "child_dof_position": [], "cog": [0,0,0], "dof_with_parent": [], "extended_dof": 0, "MoI": [[1,0,0],[0,1,0],[0,0,1]], "mass": 1e6, "mesh": "./test/business/data/Cylinder.dat", "mesh_format": "dat", "number_panel": 500, "number_vertex": 2000, "number_child": 0, "parent_body": [-1]}]}' | http --print=HBhb --check-status put mc:5000/mc/1/model/wec/complexity3
	echo '{"id": 1}' | http --print=HBhb --check-status mc:5000/mc/tasks

x-check-commit:
	pipenv run pre-commit run --all-files

x-check-pylint:
	pipenv run pylint dtop_shared_library

x-check-cov:
	pipenv run pytest --cov=dtop_shared_library --cov-fail-under=100 --cov-report term-missing tests

x-dev-install:
	pipenv run python setup.py develop

x-dev-uninstall:
	pipenv run python setup.py develop --uninstall

x-pip-env:
	pipenv install --dev

x-pip-run:
	pipenv shell

x-pip-rm:
	pipenv --rm

x-conda-create: x-conda-rm
	conda create --prefix envs pip make compilers --channel conda-forge

x-conda-rm:
	rm -fr envs

x-gitlab-ci: x-pip-env check

gitlab-ci:
	docker build .
