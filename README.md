# Machine Characterization Overview

Machine Characterization Module of the DTOceanPlus project.

**Important** that this repository uses other functionality via [Git Submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) feature and needs to be cloned in a special way

```bash
git clone --recurse-submodules <repo URL>
```

If you already have a cloned local repositiry, then you have to update it in the following way:

```bash
git submodule update --init --recursive
```

## Running the module using with Docker (PREFERRED METHOD)

Install [Docker]

[Docker]: https://www.docker.com/products/docker-desktop

To start the container

```bash
cd PROJECT_ROOT
docker-compose up
```

The first time the container need to be built, this will take up to 5 minutes.

The project front page can be found at [mc-dashboard]

[mc-dashboard]: http://localhost:5000/

> **NOTE**: if after adding the project using the add form, the project is not listed in the dashboad the DB was not generated properly. It is possible to solve the problem for the time being by creating the dtop_machine environment from the dependency.yml file, activate the environment and issue the flask init-db command on a terminal

To stop the services (for all OS) use `Ctrl+C`

If needed the container can be removed using the following command
(the services are named `dtop-mc:0.0.1`)

```bash
docker rm $(docker stop $(docker ps -a -q --filter ancestor=dtop-mc:0.0.1 --format="{{.ID}}"))
```

> **NOTE**: it is important to clean the docker images and volumes, please check this instructions [docker-clean]

[docker-clean]: https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/

The client page can be found at localhost:8000

Documentation:

- [Python Packaging User Guide](https://packaging.python.org)
- [Installing Packages](https://packaging.python.org/tutorials/installing-packages/)
- [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

## Testing

See [test](test) folder.

The [pytest](https://docs.pytest.org/en/latest/) framework makes it easy to write tests.

Install pytest:

```bash
pip install pytest
```

Run tests:

```bash
python -m pytest
```

### Dredd

To kill an hanging flask server as a result of a dredd failure, it is possible to list the services
that are listening the port and kill it

```bash
lsof -i :5000 | grep LISTEN
kill <process_id>
```

the process ID is the second item printed from lsof

If dredd fails to build the hooks try to issue the following command to have a better view on the error

```bash
dredd-hooks-python ABSOLUTE_PATH_TO_HOOKS.py
```

### E2E Testing (Cypress)

End-to-End (E2E) testing is one of final valuable indicators of a module progress and a quality.
However, this can be quite time consuming and depending on a stable configuration.

For achievement of good balance between unit tests and E2E tests, it is desirable to provide a sufficient amount of unit tests.

Use the next command for running tests
```bash
make --file mc.makefile frontend-e2e-test
```

To get more technical details, please consider reading the dedicated [wiki page](https://gitlab.com/opencascade/dtocean/wiki/-/blob/master/dtoceanplus/E2E-tests-via-Cypress.md)

#### Using Cypress as a Development Tool

To launch the Cypress App on Electron type the following
```bash
cd e2e-cypress
$(npm bin)/cypress open
```
If a verify error happen
```
 ✖  Verifying Cypress can run /Users/ff/Library/Caches/Cypress/4.5.0/Cypress.app
    → Cypress Version: 4.5.0
Cypress verification timed out.

This command failed with the following output:

/Users/ff/Library/Caches/Cypress/4.5.0/Cypress.app/Contents/MacOS/Cypress --no-sandbox --smoke-test --ping=391

----------

Command failed: /Users/ff/Library/Caches/Cypress/4.5.0/Cypress.app/Contents/MacOS/Cypress --no-sandbox --smoke-test --ping=391

----------

Platform: darwin (19.4.0)
Cypress Version: 4.5.0
```
try to open cypress again, cos the OS is performing a verificaiton that last more than the Cypress timeout.

If the client has not been started yet, a warning will appear in the Cypress page.
Start the client using the command

```docker
docker-compose -f docker-compose.yml -f e2e.docker-compose.yml up client mc worker
```

### Coverage

Coverage reports about code which should be tested.

Python has tool [Coverage.py](https://coverage.readthedocs.io/)
and PyTest has plugin for it [pytest-cov](https://pytest-cov.readthedocs.io/).

Install pytest-cov plugin

```bash
pip install pytest-cov
```

Run pytest with coverage:

```bash
python -m pytest --cov=dtop_energycapt
```

## Languages

Python has tool [Babel](http://babel.pocoo.org/) that assist in internationalizing and localizing
and Flask has plugin for it [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

See [babel.cfg](babel.cfg) file.

Extracting Text to Translate:

```bash
pybabel extract .
```

It will create `message.pot` file with translations.
This file should not be stored in repository.

Generating a Language Catalog:

```bash
pybabel init -i messages.pot -d src/dtop_energycapt/service/translations -l fr
```

It will create [src/dtop_energycapt/service/translations/fr/LC_MESSAGES/messages.po](src/service/gui/translations/fr/LC_MESSAGES/messages.po) text file.

Use tool [poEdit](https://www.poedit.net/) for catalog (.po file) editing.

Compile Catalog:

```bash
pybabel compile -d src/dtop_energycapt/service/translations
```

It will create `src/dtop_energycapt/service/translations/fr/LC_MESSAGES/messages.mo` binary file.
This file is used by [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

Tools:

- [poEdit](https://www.poedit.net/)

Documentation:

- [I18n and L10n](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xiii-i18n-and-l10n)
- [Babel](http://babel.pocoo.org/)
- [Flask-Babel](https://pythonhosted.org/Flask-Babel/)

## Service API documentation

For service API description there is [OpenAPI Specification](https://openapis.org).
See [openapi.json](src/dtop_energycapt/service/static/openapi.json).

For API documentation page there is [ReDoc](https://github.com/Rebilly/ReDoc) tool.
It is OpenAPI/Swagger-generated API Reference Documentation.
See [templates/api/index.html](src/dtop_energycapt/service/templates/api/index.html) and
[api/foo/__init__.py](src/dtop_energycapt/service/api/foo/__init__.py).

For generating OpenAPI files from code comments there is tool [oas](https://openap.is).

Install oas with [Node.js npm](https://nodejs.org/):

```bash
npm install oas -g
```

Init template:

```bash
oas init
```

See [swagger.yaml](swagger.yaml).

Generate OpenAPI description:

```bash
oas generate > src/dtop_energycapt/service/static/openapi.json
```

See [openapi.json](src/dtop_energycapt/service/static/openapi.json).

Documentation:

- [ReDoc](https://github.com/Rebilly/ReDoc)
- [OpenAPI Specification by Swagger](https://swagger.io/specification/)
- [oas](https://openap.is)
- [OpenAPI Tools](https://openapi.tools)

## GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file tells the GitLab runner what to do.

GitLab offers a CI (Continuous Integration) service.
CI is for build and check each modification in repository.

Documentation:

- [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)

## Start Service

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

It is recommended to create the file `.flaskenv` at the highest folder level
The `.flaskenv` file should contain

```DOS .bat
FLASK_APP=dtop_energycapt.service
FLASK_ENV=development
FLASK_RUN_PORT=5000
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_energycapt.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```

and issue the following command

```DOS .bat
export FLASK_APP=src.dtop_energycapt.service
```

Initialize database:

```DOS .bat
flask init-db
```

Database will be created in `instance/dtop_main.sqlite`.

Start the service:

```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.

## Additional info for Running and Testing the module manually

The next commands can be executed to run and test the module manually.

Cleanup - deactivate / remove conda environment if necessary :

```DOS .bat
conda deactivate
conda remove --name dtop_machine_env --all
```

Create and activate conda environment :

```DOS .bat
conda env create --file dependency.yml
conda activate dtop_machine_env
```

Modify environment :

  1. Adjust [conda-packages.txt](conda-packages.txt) and [pip-packages.txt](pip-packages.txt) files
  1. Enter the container by executing `make x-be-dev` command
  1. Run the `source ./dependency-update.sh` command-line utility inside
  1. Check the modified [dependency.yml](dependency.yml]) file

Install the module :

```DOS .bat
pip install -e .
```

Build module frontend :

```DOS .bat
cd src/dtop_machine/frontend
npm install
npm run build:prod
cd ../../..
```

Initiate database and run application directly

```DOS .bat
flask init-db
flask run
```

Or using `docker-compose`

```bash
docker-compose up
```

`docker-compose up` starts the module container and executes both the existing unit and dredd tests and continues to be running.

Now you can open the module in browser this URL: http://localhost:5000/.

### Test Environment

#### Windows

#### OS version

```DOS .bat
Windows 10 Pro Version 1903
```

#### Docker Version

```DOS .bat
Docker version 19.03.5, build 633a0ea
```

#### Minicanda3 Version

```DOS .bat
conda 4.8.2
```
