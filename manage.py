#!/usr/bin/env python
import json

import redis
from flask.cli import FlaskGroup
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry

from src.dtop_machine.service import create_app, models
from src.dtop_machine.service.models import Task, db

import logging

# # manual config
import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')
# logging.basicConfig(filename='/logs/mc.log', filemode='w',
#                      format='%(asctime)s :: %(name)s - %(levelname)s - %(message)s')
# logger = logging.getLogger()
# logger.setLevel(logging.INFO)
# file_handler = RotatingFileHandler('/logs/mc.log','w', 1000000,1)
# formatter = logging.Formatter('%(asctime)s :: %(name)s - %(levelname)s :: %(message)s', '%Y-%m-%d %H:%M:%S')
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

# log file config
from dtop_machine.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger()

logger.warning('Started worker')

app = create_app()
cli = FlaskGroup(create_app=create_app)
redis_url = app.config["REDIS_URL"]
redis_connection = redis.from_url(redis_url)

@cli.command("run_worker")
def run_worker():
    with Connection(redis_connection):
        worker = Worker(app.config["QUEUES"])
        worker.work()


if __name__ == "__main__":
    cli()
